﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleship
{
    public partial class MatchList : Form
    {
        Dictionary<string, string> list = new Dictionary<string, string>();
        public MatchList()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e) // Join
        {
            byte[] buffer = new byte[2048];
            String selectedUser = (String) listBox1.SelectedItem;
            Socket sock = UDPClass.sendUDPMessageToPlace(list[selectedUser]+"/");

            EndPoint point2 = new IPEndPoint(IPAddress.Any, 5500);
            sock.ReceiveFrom(buffer, ref point2);
            String[] data = GetStringFromBuffer(buffer).Split('/');

            ConnectTCP(IPAddress.Parse(data[1]), Int32.Parse(data[2]));
            GameBoard client = new GameBoard();

            client.Show();
            this.Hide();
            sock.Close();

        }

        private void button3_Click(object sender, EventArgs e) // Host
        {
            byte[] buffer = new byte[2048];
            Socket sock = UDPClass.sendUDPMessageToServer("Host/"+ (String)Properties.Settings.Default["name"]+"/"+ TCPClassServer.findOpenPort()+"/");
            EndPoint point2 = new IPEndPoint(IPAddress.Any, 5201);
            sock.ReceiveFrom(buffer, ref point2);
            String data = GetStringFromBuffer(buffer);
            if (data == "HostRecieved")
            {
                GameLogic.setHost();
                Console.WriteLine(GameLogic.netMode);
                var task = Task.Run(() => OpenTCP());
                TCPClassServer.ConnectionallDone.WaitOne();
                GameBoard host = new GameBoard();
                host.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Failed to Connect", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
        }

        private void button1_Click(object sender, EventArgs e) // Close
        {

            Application.Exit();
        }

        private void MatchList_Shown(object sender, EventArgs e)
        {
            byte[] buffer = new byte[2048];
            Socket socket = UDPClass.sendUDPMessageToServer("ListGames/");
            EndPoint point2 = new IPEndPoint(IPAddress.Any, 5201);
            socket.ReceiveFrom(buffer, ref point2);
            string a = GetStringFromBuffer(buffer);
            if(a == "")
            {
                socket.Close();
                return; 
            }
            String[] data = a.Split(':');
            listBox1.Items.Clear();
            for(int item = 0; item < data.Length-1; item+=2)
            {
                listBox1.Items.Add(data[item+1]);
                list.Add(data[item + 1], data[item]);
            }
            Console.WriteLine(list);
            socket.Close();

        }
         /*GetStringFromBuffer
         * Takes in byte buffer and converts it into a string by finding hte first trailing 
         * 0 byte on the end of the array and convert everything before into a string.
         * 
         * Input: byte[] buffer
         * output: String 
         */
        private static String GetStringFromBuffer(byte[] buffer)
        {
            if (buffer[0] == 0)
            {
                return "";
            }
            //Gets the String from a byte buffer. Removes all trailing 0 bytes.
            int i = buffer.Length - 1;
            //Find first non zero byte
            while (buffer[i] == 0)
                --i;
            //create a temporary byte array to store the contents of the byte string
            byte[] RetVal = new byte[i + 1];
            Array.Copy(buffer, RetVal, i + 1);
            return Encoding.ASCII.GetString(RetVal); ;

        }
        private void OpenTCP()
        {
            Console.WriteLine(GameLogic.netMode);
            TCPClassServer.OpenPort();
        }
        private void ConnectTCP(IPAddress host, int port)
        {
            TCPClassClient.CreateConnection(host, port);

        }
        private static IPAddress parseIP(string passedIn)
        {
            //Seperates an IP from Port string. For Example 1.2.3.4:123 = 1.2.3.4
            return IPAddress.Parse(passedIn.Split(":".ToCharArray(), 2)[0]);
        }
    }
}
