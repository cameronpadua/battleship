﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Battleship
{
    public static class UDPClass
    {
        public static IPAddress serverIP = IPAddress.None;
        static int serverPort = 0;
        private static bool isFirst = true;
        private static bool isFirst2 = true;
        public static void setServerIPAndPort(IPAddress server, int port)
        {
            serverIP = server;
            serverPort = port;
        }
        public static Socket sendUDPMessageToServer(String command, String data = "")
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            EndPoint remoteEP = new IPEndPoint(serverIP, serverPort);
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 5000);
            if(isFirst)
            socket.Bind(new IPEndPoint(IPAddress.Any, 5201));
            isFirst = false;
            String send = command + data;
            socket.SendTo(Encoding.ASCII.GetBytes(send), remoteEP);
            return socket;
        }
        public static Socket sendUDPMessageToPlace(String command, String data = "")
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            EndPoint remoteEP = new IPEndPoint(serverIP, 5500);
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 5000);
            if (isFirst2)
                socket.Bind(new IPEndPoint(IPAddress.Any, 5500));
            isFirst2 = false;
            String send = command + data;
            socket.SendTo(Encoding.ASCII.GetBytes(send), remoteEP);
            return socket;
        }
        public static string GetStringFromBuffer(byte[] buffer)
        {
            int index = buffer.Length - 1;
            while (buffer[index] == 0)
            {
                index--;
            }
            byte[] destinationArray = new byte[index + 1];
            Array.Copy(buffer, destinationArray, (int)(index + 1));
            return Encoding.ASCII.GetString(destinationArray);
        }
    }
}
