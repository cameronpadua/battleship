﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;

    // State object for reading client data asynchronously  
    public class StateObject
    {
        // Client  socket.  
        public Socket workSocket = null;
        // Size of receive buffer.  
        public const int BufferSize = 1024;
        // Receive buffer.  
        public byte[] buffer = new byte[BufferSize];
        // Received data string.  
        public StringBuilder sb = new StringBuilder();
    }

    public class TCPClassServer
    {
        // Thread signal.  
        public static ManualResetEvent ConnectionallDone = new ManualResetEvent(false);
        public static ManualResetEvent receiveDone = new ManualResetEvent(false);
        public static ManualResetEvent setDone = new ManualResetEvent(false);
        public static bool connectionMade = false;
        public static Socket Serverhandler;
        public static StateObject state;
        public static int myport = 0;

        public TCPClassServer()
        {
        }
        public static bool checkStatus(Socket client)
        {
            if(client.Poll(10, SelectMode.SelectRead))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static void Receive(Socket client)
        {
            try
            {

                // Begin receiving the data from the remote device.  
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public static void OpenPort()
        {
            IPAddress ipAddress = IPAddress.None;
            var host = Dns.GetHostEntry(Dns.GetHostName());
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 0);
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                        localEndPoint = new IPEndPoint(ip, myport);
                }
            }
            // Create a TCP/IP socket.  
            Socket listener = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            // Bind the socket to the local endpoint and listen for incoming connections.  
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(1);
                // Set the event to nonsignaled state.  
                ConnectionallDone.Reset();

                // Start an asynchronous socket to listen for connections.  
                Console.WriteLine("Waiting for a connection...");
                // Create the state object.  
                state = new StateObject();
                listener.BeginAccept(
                        new AsyncCallback(AcceptCallback),
                        listener);

                    // Wait until a connection is made before continuing.  
                    ConnectionallDone.WaitOne();
                

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();

        }

        public static void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.  
            ConnectionallDone.Set();

            // Get the socket that handles the client request.  
            Socket listener = (Socket)ar.AsyncState;
            Serverhandler = listener.EndAccept(ar);
            connectionMade = true;


            state.workSocket = Serverhandler;
            //Serverhandler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
               // new AsyncCallback(ReadCallback), state);
        }

        public static void ReadCallback(IAsyncResult ar)
        {

            // Retrieve the state object and the handler socket  
            // from the asynchronous state object.  
            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;

            // Read data from the client socket.   
            int bytesRead = handler.EndReceive(ar);

            if (bytesRead > 0)
            {
                // There  might be more data, so store the data received so far.  
                state.sb.Append(Encoding.ASCII.GetString(
                    state.buffer, 0, bytesRead));
                Array.Clear(state.buffer, 0, state.buffer.Length);
                
                Console.WriteLine(state.sb.ToString());
                receiveDone.Set();
            }
        }

        public static void Send(String data)
        {
            // Convert the string data to byte data using ASCII encoding.  
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.  
            Serverhandler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), Serverhandler);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.  
                Socket handler = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.  
                int bytesSent = handler.EndSend(ar);
                Console.WriteLine("Sent {0} bytes to client.", bytesSent);

                setDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        public static int findOpenPort()
        {
            //find an avaiable port
            using (var tempSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
            {
                tempSocket.Bind(new IPEndPoint(IPAddress.Parse("0.0.0.0"), port: 0));
                myport = ((IPEndPoint)tempSocket.LocalEndPoint).Port;
                return myport;
            }
        }
    }
}
