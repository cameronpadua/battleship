﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship
{
    class GameLogic
    {

        public static bool netMode = false; // Mode setting whether to use TCPClient or TCPServer, true for server, false for client.
        private GameBoard gameBoard; 
        public GameBoard.TileType[,]player;
        public GameBoard.TileType[,] opponent;

        public int playerScore = 0;
        public int opponentScore = 0;
        public int myHits = 0;
        public int maxRMoves = 100;
        public int notHit = 100;

        public int localShipCount = 0;
        public static readonly int MAX_SHIPS = 17;

        public GameLogic(bool networkMode, GameBoard myBoard)
        {
            gameBoard = myBoard;
            player = new GameBoard.TileType[10,10];
            opponent = new GameBoard.TileType[10, 10];

            for(int i = 0; i < 10; i++)
            {
                for(int j = 0; j < 10; j++)
                {
                    player[i, j] = GameBoard.TileType.Empty;
                    opponent[i, j] = GameBoard.TileType.Empty;
                }
            }
        }
        public static void setHost()
        {
            netMode = true;
        }
        public bool setShip(int x, int y)
        {
            if(gameBoard.getPlayerShipType(x,y) == GameBoard.TileType.Empty)
            {
                localShipCount++;
                gameBoard.setPlayerShipType(x, y, GameBoard.TileType.Ship);
                player[y, x] = GameBoard.TileType.Ship;
                return true;
            } else if(gameBoard.getPlayerShipType(x, y) == GameBoard.TileType.Ship)
            {
                localShipCount--;
                gameBoard.setPlayerShipType(x, y, GameBoard.TileType.Empty);
                player[y, x] = GameBoard.TileType.Empty;
                return true;
            }
            return false;
        }

        public bool makeMove(int x, int y, int m, int s)
        {
            if (gameBoard.getOpponentShipType(x, y) == GameBoard.TileType.Empty)
            {
                gameBoard.addAction("You missed!");
                gameBoard.setOpponentShipType(x, y, GameBoard.TileType.Miss);
                opponent[y, x] = GameBoard.TileType.Miss;
                calculateScore(m, s, GameBoard.TileType.Miss);

                if (netMode)
                {
                    TCPClassServer.Send("S"+":"+y+":"+x+":"+playerScore);
                }
                else
                {
                    TCPClassClient.Send("S" + ":" + y + ":" + x + ":" + playerScore );
                }
                gameBoard.updatePowerUps();
                return true;
            }

            if (gameBoard.getOpponentShipType(x, y) == GameBoard.TileType.Ship)
            {
                gameBoard.addAction("You hit their ship!");
                gameBoard.setOpponentShipType(x, y, GameBoard.TileType.Hit);
                opponent[y, x] = GameBoard.TileType.Hit;
                calculateScore(m, s, GameBoard.TileType.Hit);
                myHits++;
                if (netMode)
                {
                    TCPClassServer.Send("S" + ":" + y + ":" + x + ":" + playerScore );
                }
                else
                {
                    TCPClassClient.Send("S" + ":" + y + ":" + x + ":" + playerScore);
                }
                gameBoard.updatePowerUps();
                return true;
            }

            if (gameBoard.getOpponentShipType(x, y) == GameBoard.TileType.Mine)
            {
                gameBoard.addAction("You hit a mine!");
                gameBoard.setOpponentShipType(x, y, GameBoard.TileType.Mine);
                opponent[y, x] = GameBoard.TileType.Mine;
                calculateScore(m, s, GameBoard.TileType.Mine);

                if (netMode)
                {
                    TCPClassServer.Send("S" + ":" + y + ":" + x + ":" + playerScore );
                }
                else
                {
                    TCPClassClient.Send("S" + ":" + y + ":" + x + ":" + playerScore );
                }
                gameBoard.updatePowerUps();
                return true;
            }


            return false;
        }

        public bool useMine() 
        {
            Random r = new Random();
            int remain = 5;
            int aTiles = 0;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if(player[i,j] == GameBoard.TileType.Empty)
                    {
                        aTiles++;
                    }
                    player[i, j] = GameBoard.TileType.Empty;
                }
            }
            if(aTiles <= 15)
            {
                return false;
            }
            int[] mineLocations = new int[10];
            int curr = 0;
            while (remain > 0) 
            {
                int randX = r.Next(0, 10);
                int randY = r.Next(0, 10);
                if (player[randY, randX] == GameBoard.TileType.Empty)
                {
                    player[randY, randX] = GameBoard.TileType.Mine;
                    gameBoard.setPlayerShipType(randX, randY, GameBoard.TileType.Mine);
                    mineLocations[curr] = randY;
                    mineLocations[curr+1] = randX;
                    curr += 2;
                    remain--;
                    maxRMoves--;
                }
            }
            playerScore -= 800;
            gameBoard.updatePowerUps();
            string send = "M" + ":";
            foreach (int i in mineLocations)
            {
                send += mineLocations[i] + ":";
            }
            send += playerScore;
            if (netMode)
            {
                TCPClassServer.Send(send);
            }
            else
            {
                TCPClassClient.Send(send);
            }
            gameBoard.addAction("You deployed mines!");
            return true;
        }

        public bool useAirStrike(int x)
        {
            for(int i = 0; i < 10; i++)
            {
                if (opponent[i, x] == GameBoard.TileType.Ship)
                {
                    opponent[i, x] = GameBoard.TileType.Hit;
                    gameBoard.setOpponentShipType(x, i, GameBoard.TileType.Hit);
                    myHits++;
                }
                else if (opponent[i, x] == GameBoard.TileType.Empty)
                {
                    opponent[i, x] = GameBoard.TileType.Miss;
                    gameBoard.setOpponentShipType(x, i, GameBoard.TileType.Miss);
                }
                else if (opponent[i, x] == GameBoard.TileType.Mine)
                {
                    opponent[i, x] = GameBoard.TileType.Miss;
                    gameBoard.setOpponentShipType(x, i, GameBoard.TileType.Miss);
                }

              
                
            }
            maxRMoves -= 10;
            playerScore -= 1280;
            gameBoard.updatePowerUps();
            if (netMode)
            {
                TCPClassServer.Send("A" + ":" + x + ":" + playerScore);
            }
            else
            {
                TCPClassClient.Send("A" + ":" + x + ":" + playerScore);
            }
            gameBoard.addAction("You used an airstrike!");
            return true;
        }

        public bool useRadar()
        {
            Random r = new Random();
            while (true)
            { 
                int randX = r.Next(0, 10);
                int randY = r.Next(0, 10);
                if (opponent[randY, randX] == GameBoard.TileType.Ship)
                {
                    opponent[randY, randX] = GameBoard.TileType.Ship;
                    gameBoard.setOpponentShipType(randX, randY, GameBoard.TileType.Ship);
                    break;
                }
            }
            gameBoard.updatePowerUps();
            if (netMode)
            {
                TCPClassServer.Send("R" + ":" + playerScore);
            }
            else
            {
                TCPClassClient.Send("R" + ":" + playerScore);
            }
            playerScore -= 400;
            gameBoard.addAction("You used radar!");
            return true;
        }

        public bool verifyCount()
        {
            if(localShipCount != MAX_SHIPS)
            {
                return false;
            }
            return true;
        }


        public void exchangeBoards()
        {
            //host
            
            if (netMode)
            {
                TCPClassServer.Receive(TCPClassServer.state.workSocket);
                TCPClassServer.receiveDone.WaitOne();
                TCPClassServer.receiveDone.Reset();
                string sendLocations = "";
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        if(player[i, j] == GameBoard.TileType.Ship)
                        {
                            sendLocations += i + ":" + j + ":";
                        }
                    }
                }
                TCPClassServer.Send((sendLocations));
                getShipsFromExchange();
                TCPClassServer.state.sb.Clear();
            }
            else
            {

                string sendLocations = "";
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        if (player[i, j] == GameBoard.TileType.Ship)
                        {
                            sendLocations += i + ":" + j + ":";
                        }
                    }
                }
                TCPClassClient.Send((sendLocations));
                TCPClassClient.Receive(TCPClassClient.client);
                TCPClassClient.receiveDone.WaitOne();
                TCPClassClient.receiveDone.Reset();
                getShipsFromExchange();
                TCPClassClient.state.sb.Clear();
            }
        }
        private void getShipsFromExchange()
        {
            if (netMode)//host
            {
                String [] segements = TCPClassServer.state.sb.ToString().Split(':');
                for(int current = 0; current < segements.Length-1; current += 2)
                {
                    opponent[Int32.Parse(segements[current]), Int32.Parse(segements[current+1])] = GameBoard.TileType.Ship;
                    //gameBoard.setOpponentShipType(Int32.Parse(segements[current + 1]), Int32.Parse(segements[current]), GameBoard.TileType.Ship);
                }
                
            }
            else
            {
                String[] segements = TCPClassClient.state.sb.ToString().Split(':');
                for (int current = 0; current < segements.Length-1; current += 2)
                {
                    opponent[Int32.Parse(segements[current]), Int32.Parse(segements[current + 1])] = GameBoard.TileType.Ship;
                    //gameBoard.setOpponentShipType(Int32.Parse(segements[current + 1]), Int32.Parse(segements[current]), GameBoard.TileType.Ship);
                }
            }
        }
    

        public void calculateScore(int m, int s, GameBoard.TileType tileHit)
        {
            switch (tileHit)
            {
                case GameBoard.TileType.Miss:
                    int i = m;
                    i *= 60;
                    i += s;
                    i /= 2;
                    playerScore += i;
                    break;
                case GameBoard.TileType.Hit:
                    int j = m;
                    j *= 60;
                    j += s;
                    j += 100;
                    playerScore += j;
                    break;
                case GameBoard.TileType.Mine:
                    playerScore -= 100;
                    break;
            }
            Console.WriteLine(playerScore);
        }

    }
}
