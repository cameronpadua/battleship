﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Battleship
{
    public partial class MatchmakingScreen : Form
    {
        public MatchmakingScreen()
        {
            InitializeComponent();
        }
        private void MatchmakingScreen_Show(object sender, EventArgs e)
        {
            String command = matchmaking();
            if (command.Split('/')[0] == "OpenPort")
            {
                GameLogic.setHost();
                Console.WriteLine(GameLogic.netMode);
                var task = Task.Run(() => OpenTCP());

                //if (!task.Wait(TimeSpan.FromSeconds(60)) && TCPClassServer.connectionMade == false)  
                //{
                //    Socket socket = UDPClass.sendUDPMessageToServer("Disconnect/");
                //    MessageBox.Show("No players could be found.", "Connection Failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                //    Application.Exit();
                //}
                //else
                //{
                TCPClassServer.ConnectionallDone.WaitOne();
                    GameBoard host = new GameBoard();
                    
                    host.Show();
                    this.Hide();
                //}
            }else if (command.Split('/')[0] == "ConnectTo")
            {
                ConnectTCP(IPAddress.Parse(command.Split('/')[1]), Int32.Parse(command.Split('/')[2]));
                GameBoard client = new GameBoard();
                
                client.Show();
                this.Hide();
            }


        }
        private String matchmaking()
        {
            int port = TCPClassServer.findOpenPort();
            Socket socket = UDPClass.sendUDPMessageToServer("QuickConnection/"+port+"/");
            byte[] buffer = new byte[1024];
            
            try
            {
                EndPoint point = new IPEndPoint(IPAddress.Any, 5201);
                socket.ReceiveFrom(buffer, ref point);
                Console.Write("Client got '" + UDPClass.GetStringFromBuffer(buffer) + "' from " + point.ToString() + Environment.NewLine);
            }
            catch
            {
                throw new Exception("Request not Received by Server");
            }
            
            return UDPClass.GetStringFromBuffer(buffer);
            
        }
        private void OpenTCP()
        {
            Console.WriteLine(GameLogic.netMode);
            TCPClassServer.OpenPort();
        }
        private void ConnectTCP(IPAddress host, int port)
        {
            TCPClassClient.CreateConnection(host, port);

        }
        private static IPAddress parseIP(string passedIn)
        {
            //Seperates an IP from Port string. For Example 1.2.3.4:123 = 1.2.3.4
            return IPAddress.Parse(passedIn.Split(":".ToCharArray(), 2)[0]);
        }

        private void MatchmakingScreen_Load(object sender, EventArgs e)
        {

        }
    }
}
            //Send test data to the remote device.
            //TCPClassClient.Send("This is a test");
            //TCPClassClient.sendDone.WaitOne();

//             Receive the response from the remote device.
//            TCPClassClient.Receive(TCPClassClient.client);
//            TCPClassClient.receiveDone.WaitOne();

//             Write the response to the console.
//            Console.WriteLine("Response received : {0}", TCPClassClient.response);