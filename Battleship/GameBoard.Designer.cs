﻿namespace Battleship
{
    partial class GameBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameBoard));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button91 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button42 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button44 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.button50 = new System.Windows.Forms.Button();
            this.button51 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.button53 = new System.Windows.Forms.Button();
            this.button54 = new System.Windows.Forms.Button();
            this.button55 = new System.Windows.Forms.Button();
            this.button56 = new System.Windows.Forms.Button();
            this.button57 = new System.Windows.Forms.Button();
            this.button58 = new System.Windows.Forms.Button();
            this.button59 = new System.Windows.Forms.Button();
            this.button60 = new System.Windows.Forms.Button();
            this.button61 = new System.Windows.Forms.Button();
            this.button62 = new System.Windows.Forms.Button();
            this.button63 = new System.Windows.Forms.Button();
            this.button64 = new System.Windows.Forms.Button();
            this.button65 = new System.Windows.Forms.Button();
            this.button66 = new System.Windows.Forms.Button();
            this.button67 = new System.Windows.Forms.Button();
            this.button68 = new System.Windows.Forms.Button();
            this.button69 = new System.Windows.Forms.Button();
            this.button70 = new System.Windows.Forms.Button();
            this.button71 = new System.Windows.Forms.Button();
            this.button72 = new System.Windows.Forms.Button();
            this.button73 = new System.Windows.Forms.Button();
            this.button74 = new System.Windows.Forms.Button();
            this.button75 = new System.Windows.Forms.Button();
            this.button76 = new System.Windows.Forms.Button();
            this.button77 = new System.Windows.Forms.Button();
            this.button78 = new System.Windows.Forms.Button();
            this.button79 = new System.Windows.Forms.Button();
            this.button80 = new System.Windows.Forms.Button();
            this.button81 = new System.Windows.Forms.Button();
            this.button82 = new System.Windows.Forms.Button();
            this.button83 = new System.Windows.Forms.Button();
            this.button84 = new System.Windows.Forms.Button();
            this.button85 = new System.Windows.Forms.Button();
            this.button86 = new System.Windows.Forms.Button();
            this.button87 = new System.Windows.Forms.Button();
            this.button88 = new System.Windows.Forms.Button();
            this.button89 = new System.Windows.Forms.Button();
            this.button90 = new System.Windows.Forms.Button();
            this.button92 = new System.Windows.Forms.Button();
            this.button93 = new System.Windows.Forms.Button();
            this.button94 = new System.Windows.Forms.Button();
            this.button95 = new System.Windows.Forms.Button();
            this.button96 = new System.Windows.Forms.Button();
            this.button97 = new System.Windows.Forms.Button();
            this.button98 = new System.Windows.Forms.Button();
            this.button99 = new System.Windows.Forms.Button();
            this.button100 = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.button101 = new System.Windows.Forms.Button();
            this.button102 = new System.Windows.Forms.Button();
            this.button103 = new System.Windows.Forms.Button();
            this.button104 = new System.Windows.Forms.Button();
            this.button105 = new System.Windows.Forms.Button();
            this.button106 = new System.Windows.Forms.Button();
            this.button107 = new System.Windows.Forms.Button();
            this.button108 = new System.Windows.Forms.Button();
            this.button109 = new System.Windows.Forms.Button();
            this.button110 = new System.Windows.Forms.Button();
            this.button111 = new System.Windows.Forms.Button();
            this.button112 = new System.Windows.Forms.Button();
            this.button113 = new System.Windows.Forms.Button();
            this.button114 = new System.Windows.Forms.Button();
            this.button115 = new System.Windows.Forms.Button();
            this.button116 = new System.Windows.Forms.Button();
            this.button117 = new System.Windows.Forms.Button();
            this.button118 = new System.Windows.Forms.Button();
            this.button119 = new System.Windows.Forms.Button();
            this.button120 = new System.Windows.Forms.Button();
            this.button121 = new System.Windows.Forms.Button();
            this.button122 = new System.Windows.Forms.Button();
            this.button123 = new System.Windows.Forms.Button();
            this.button124 = new System.Windows.Forms.Button();
            this.button125 = new System.Windows.Forms.Button();
            this.button126 = new System.Windows.Forms.Button();
            this.button127 = new System.Windows.Forms.Button();
            this.button128 = new System.Windows.Forms.Button();
            this.button129 = new System.Windows.Forms.Button();
            this.button130 = new System.Windows.Forms.Button();
            this.button131 = new System.Windows.Forms.Button();
            this.button132 = new System.Windows.Forms.Button();
            this.button133 = new System.Windows.Forms.Button();
            this.button134 = new System.Windows.Forms.Button();
            this.button135 = new System.Windows.Forms.Button();
            this.button136 = new System.Windows.Forms.Button();
            this.button137 = new System.Windows.Forms.Button();
            this.button138 = new System.Windows.Forms.Button();
            this.button139 = new System.Windows.Forms.Button();
            this.button140 = new System.Windows.Forms.Button();
            this.button141 = new System.Windows.Forms.Button();
            this.button142 = new System.Windows.Forms.Button();
            this.button143 = new System.Windows.Forms.Button();
            this.button144 = new System.Windows.Forms.Button();
            this.button145 = new System.Windows.Forms.Button();
            this.button146 = new System.Windows.Forms.Button();
            this.button147 = new System.Windows.Forms.Button();
            this.button148 = new System.Windows.Forms.Button();
            this.button149 = new System.Windows.Forms.Button();
            this.button150 = new System.Windows.Forms.Button();
            this.button151 = new System.Windows.Forms.Button();
            this.button152 = new System.Windows.Forms.Button();
            this.button153 = new System.Windows.Forms.Button();
            this.button154 = new System.Windows.Forms.Button();
            this.button155 = new System.Windows.Forms.Button();
            this.button156 = new System.Windows.Forms.Button();
            this.button157 = new System.Windows.Forms.Button();
            this.button158 = new System.Windows.Forms.Button();
            this.button159 = new System.Windows.Forms.Button();
            this.button160 = new System.Windows.Forms.Button();
            this.button161 = new System.Windows.Forms.Button();
            this.button162 = new System.Windows.Forms.Button();
            this.button163 = new System.Windows.Forms.Button();
            this.button164 = new System.Windows.Forms.Button();
            this.button165 = new System.Windows.Forms.Button();
            this.button166 = new System.Windows.Forms.Button();
            this.button167 = new System.Windows.Forms.Button();
            this.button168 = new System.Windows.Forms.Button();
            this.button169 = new System.Windows.Forms.Button();
            this.button170 = new System.Windows.Forms.Button();
            this.button171 = new System.Windows.Forms.Button();
            this.button172 = new System.Windows.Forms.Button();
            this.button173 = new System.Windows.Forms.Button();
            this.button174 = new System.Windows.Forms.Button();
            this.button175 = new System.Windows.Forms.Button();
            this.button176 = new System.Windows.Forms.Button();
            this.button177 = new System.Windows.Forms.Button();
            this.button178 = new System.Windows.Forms.Button();
            this.button179 = new System.Windows.Forms.Button();
            this.button180 = new System.Windows.Forms.Button();
            this.button181 = new System.Windows.Forms.Button();
            this.button182 = new System.Windows.Forms.Button();
            this.button183 = new System.Windows.Forms.Button();
            this.button184 = new System.Windows.Forms.Button();
            this.button185 = new System.Windows.Forms.Button();
            this.button186 = new System.Windows.Forms.Button();
            this.button187 = new System.Windows.Forms.Button();
            this.button188 = new System.Windows.Forms.Button();
            this.button189 = new System.Windows.Forms.Button();
            this.button190 = new System.Windows.Forms.Button();
            this.button191 = new System.Windows.Forms.Button();
            this.button192 = new System.Windows.Forms.Button();
            this.button193 = new System.Windows.Forms.Button();
            this.button194 = new System.Windows.Forms.Button();
            this.button195 = new System.Windows.Forms.Button();
            this.button196 = new System.Windows.Forms.Button();
            this.button197 = new System.Windows.Forms.Button();
            this.button198 = new System.Windows.Forms.Button();
            this.button199 = new System.Windows.Forms.Button();
            this.button200 = new System.Windows.Forms.Button();
            this.actionLog = new System.Windows.Forms.ListBox();
            this.chat = new System.Windows.Forms.ListBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button201 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.opScoreText = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.playerScoreText = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.timeText = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.secondTick = new System.Windows.Forms.Timer(this.components);
            this.label35 = new System.Windows.Forms.Label();
            this.button202 = new System.Windows.Forms.Button();
            this.button203 = new System.Windows.Forms.Button();
            this.button204 = new System.Windows.Forms.Button();
            this.button205 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 10;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.Controls.Add(this.button91, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.button2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.button4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.button5, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.button6, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.button7, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.button8, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.button9, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.button10, 9, 0);
            this.tableLayoutPanel1.Controls.Add(this.button11, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.button12, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.button13, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.button14, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.button15, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.button16, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.button17, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.button18, 7, 1);
            this.tableLayoutPanel1.Controls.Add(this.button19, 8, 1);
            this.tableLayoutPanel1.Controls.Add(this.button20, 9, 1);
            this.tableLayoutPanel1.Controls.Add(this.button21, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.button22, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.button23, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.button24, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.button25, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.button26, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.button27, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.button28, 7, 2);
            this.tableLayoutPanel1.Controls.Add(this.button29, 8, 2);
            this.tableLayoutPanel1.Controls.Add(this.button30, 9, 2);
            this.tableLayoutPanel1.Controls.Add(this.button31, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.button32, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.button33, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.button34, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.button35, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.button36, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.button37, 6, 3);
            this.tableLayoutPanel1.Controls.Add(this.button38, 7, 3);
            this.tableLayoutPanel1.Controls.Add(this.button39, 8, 3);
            this.tableLayoutPanel1.Controls.Add(this.button40, 9, 3);
            this.tableLayoutPanel1.Controls.Add(this.button41, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.button42, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.button43, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.button44, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.button45, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.button46, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.button47, 6, 4);
            this.tableLayoutPanel1.Controls.Add(this.button48, 7, 4);
            this.tableLayoutPanel1.Controls.Add(this.button49, 8, 4);
            this.tableLayoutPanel1.Controls.Add(this.button50, 9, 4);
            this.tableLayoutPanel1.Controls.Add(this.button51, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.button52, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.button53, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.button54, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.button55, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.button56, 5, 5);
            this.tableLayoutPanel1.Controls.Add(this.button57, 6, 5);
            this.tableLayoutPanel1.Controls.Add(this.button58, 7, 5);
            this.tableLayoutPanel1.Controls.Add(this.button59, 8, 5);
            this.tableLayoutPanel1.Controls.Add(this.button60, 9, 5);
            this.tableLayoutPanel1.Controls.Add(this.button61, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.button62, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.button63, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.button64, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.button65, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this.button66, 5, 6);
            this.tableLayoutPanel1.Controls.Add(this.button67, 6, 6);
            this.tableLayoutPanel1.Controls.Add(this.button68, 7, 6);
            this.tableLayoutPanel1.Controls.Add(this.button69, 8, 6);
            this.tableLayoutPanel1.Controls.Add(this.button70, 9, 6);
            this.tableLayoutPanel1.Controls.Add(this.button71, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.button72, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.button73, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.button74, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.button75, 4, 7);
            this.tableLayoutPanel1.Controls.Add(this.button76, 5, 7);
            this.tableLayoutPanel1.Controls.Add(this.button77, 6, 7);
            this.tableLayoutPanel1.Controls.Add(this.button78, 7, 7);
            this.tableLayoutPanel1.Controls.Add(this.button79, 8, 7);
            this.tableLayoutPanel1.Controls.Add(this.button80, 9, 7);
            this.tableLayoutPanel1.Controls.Add(this.button81, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.button82, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.button83, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.button84, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.button85, 4, 8);
            this.tableLayoutPanel1.Controls.Add(this.button86, 5, 8);
            this.tableLayoutPanel1.Controls.Add(this.button87, 6, 8);
            this.tableLayoutPanel1.Controls.Add(this.button88, 7, 8);
            this.tableLayoutPanel1.Controls.Add(this.button89, 8, 8);
            this.tableLayoutPanel1.Controls.Add(this.button90, 9, 8);
            this.tableLayoutPanel1.Controls.Add(this.button92, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.button93, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.button94, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.button95, 4, 9);
            this.tableLayoutPanel1.Controls.Add(this.button96, 5, 9);
            this.tableLayoutPanel1.Controls.Add(this.button97, 6, 9);
            this.tableLayoutPanel1.Controls.Add(this.button98, 7, 9);
            this.tableLayoutPanel1.Controls.Add(this.button99, 8, 9);
            this.tableLayoutPanel1.Controls.Add(this.button100, 9, 9);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(321, 35);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(449, 449);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // button91
            // 
            this.button91.Location = new System.Drawing.Point(3, 408);
            this.button91.Name = "button91";
            this.button91.Size = new System.Drawing.Size(39, 39);
            this.button91.TabIndex = 90;
            this.button91.Text = "button91";
            this.button91.UseVisualStyleBackColor = true;
            this.button91.Click += new System.EventHandler(this.button91_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(48, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(39, 39);
            this.button2.TabIndex = 1;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(39, 39);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(93, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(39, 39);
            this.button3.TabIndex = 2;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(138, 3);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(39, 39);
            this.button4.TabIndex = 3;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(183, 3);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(39, 39);
            this.button5.TabIndex = 4;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(228, 3);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(39, 39);
            this.button6.TabIndex = 5;
            this.button6.Text = "button6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(273, 3);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(39, 39);
            this.button7.TabIndex = 6;
            this.button7.Text = "button7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(318, 3);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(39, 39);
            this.button8.TabIndex = 7;
            this.button8.Text = "button8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(363, 3);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(39, 39);
            this.button9.TabIndex = 8;
            this.button9.Text = "button9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(408, 3);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(39, 39);
            this.button10.TabIndex = 9;
            this.button10.Text = "button10";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(3, 48);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(39, 39);
            this.button11.TabIndex = 10;
            this.button11.Text = "button11";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(48, 48);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(39, 39);
            this.button12.TabIndex = 11;
            this.button12.Text = "button12";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(93, 48);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(39, 39);
            this.button13.TabIndex = 12;
            this.button13.Text = "button13";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(138, 48);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(39, 39);
            this.button14.TabIndex = 13;
            this.button14.Text = "button14";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(183, 48);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(39, 39);
            this.button15.TabIndex = 14;
            this.button15.Text = "button15";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(228, 48);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(39, 39);
            this.button16.TabIndex = 15;
            this.button16.Text = "button16";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(273, 48);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(39, 39);
            this.button17.TabIndex = 16;
            this.button17.Text = "button17";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(318, 48);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(39, 39);
            this.button18.TabIndex = 17;
            this.button18.Text = "button18";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(363, 48);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(39, 39);
            this.button19.TabIndex = 18;
            this.button19.Text = "button19";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(408, 48);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(39, 39);
            this.button20.TabIndex = 19;
            this.button20.Text = "button20";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(3, 93);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(39, 39);
            this.button21.TabIndex = 20;
            this.button21.Text = "button21";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(48, 93);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(39, 39);
            this.button22.TabIndex = 21;
            this.button22.Text = "button22";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(93, 93);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(39, 39);
            this.button23.TabIndex = 22;
            this.button23.Text = "button23";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(138, 93);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(39, 39);
            this.button24.TabIndex = 23;
            this.button24.Text = "button24";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(183, 93);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(39, 39);
            this.button25.TabIndex = 24;
            this.button25.Text = "button25";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(228, 93);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(39, 39);
            this.button26.TabIndex = 25;
            this.button26.Text = "button26";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(273, 93);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(39, 39);
            this.button27.TabIndex = 26;
            this.button27.Text = "button27";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(318, 93);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(39, 39);
            this.button28.TabIndex = 27;
            this.button28.Text = "button28";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click);
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(363, 93);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(39, 39);
            this.button29.TabIndex = 28;
            this.button29.Text = "button29";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(408, 93);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(39, 39);
            this.button30.TabIndex = 29;
            this.button30.Text = "button30";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(3, 138);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(39, 39);
            this.button31.TabIndex = 30;
            this.button31.Text = "button31";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(48, 138);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(39, 39);
            this.button32.TabIndex = 31;
            this.button32.Text = "button32";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(93, 138);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(39, 39);
            this.button33.TabIndex = 32;
            this.button33.Text = "button33";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(138, 138);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(39, 39);
            this.button34.TabIndex = 33;
            this.button34.Text = "button34";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(183, 138);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(39, 39);
            this.button35.TabIndex = 34;
            this.button35.Text = "button35";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(228, 138);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(39, 39);
            this.button36.TabIndex = 35;
            this.button36.Text = "button36";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(273, 138);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(39, 39);
            this.button37.TabIndex = 36;
            this.button37.Text = "button37";
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(318, 138);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(39, 39);
            this.button38.TabIndex = 37;
            this.button38.Text = "button38";
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.button38_Click);
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(363, 138);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(39, 39);
            this.button39.TabIndex = 38;
            this.button39.Text = "button39";
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(408, 138);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(39, 39);
            this.button40.TabIndex = 39;
            this.button40.Text = "button40";
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(3, 183);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(39, 39);
            this.button41.TabIndex = 40;
            this.button41.Text = "button41";
            this.button41.UseVisualStyleBackColor = true;
            this.button41.Click += new System.EventHandler(this.button41_Click);
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(48, 183);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(39, 39);
            this.button42.TabIndex = 41;
            this.button42.Text = "button42";
            this.button42.UseVisualStyleBackColor = true;
            this.button42.Click += new System.EventHandler(this.button42_Click);
            // 
            // button43
            // 
            this.button43.Location = new System.Drawing.Point(93, 183);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(39, 39);
            this.button43.TabIndex = 42;
            this.button43.Text = "button43";
            this.button43.UseVisualStyleBackColor = true;
            this.button43.Click += new System.EventHandler(this.button43_Click);
            // 
            // button44
            // 
            this.button44.Location = new System.Drawing.Point(138, 183);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(39, 39);
            this.button44.TabIndex = 43;
            this.button44.Text = "button44";
            this.button44.UseVisualStyleBackColor = true;
            this.button44.Click += new System.EventHandler(this.button44_Click);
            // 
            // button45
            // 
            this.button45.Location = new System.Drawing.Point(183, 183);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(39, 39);
            this.button45.TabIndex = 44;
            this.button45.Text = "button45";
            this.button45.UseVisualStyleBackColor = true;
            this.button45.Click += new System.EventHandler(this.button45_Click);
            // 
            // button46
            // 
            this.button46.Location = new System.Drawing.Point(228, 183);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(39, 39);
            this.button46.TabIndex = 45;
            this.button46.Text = "button46";
            this.button46.UseVisualStyleBackColor = true;
            this.button46.Click += new System.EventHandler(this.button46_Click);
            // 
            // button47
            // 
            this.button47.Location = new System.Drawing.Point(273, 183);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(39, 39);
            this.button47.TabIndex = 46;
            this.button47.Text = "button47";
            this.button47.UseVisualStyleBackColor = true;
            this.button47.Click += new System.EventHandler(this.button47_Click);
            // 
            // button48
            // 
            this.button48.Location = new System.Drawing.Point(318, 183);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(39, 39);
            this.button48.TabIndex = 47;
            this.button48.Text = "button48";
            this.button48.UseVisualStyleBackColor = true;
            this.button48.Click += new System.EventHandler(this.button48_Click);
            // 
            // button49
            // 
            this.button49.Location = new System.Drawing.Point(363, 183);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(39, 39);
            this.button49.TabIndex = 48;
            this.button49.Text = "button49";
            this.button49.UseVisualStyleBackColor = true;
            this.button49.Click += new System.EventHandler(this.button49_Click);
            // 
            // button50
            // 
            this.button50.Location = new System.Drawing.Point(408, 183);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(39, 39);
            this.button50.TabIndex = 49;
            this.button50.Text = "button50";
            this.button50.UseVisualStyleBackColor = true;
            this.button50.Click += new System.EventHandler(this.button50_Click);
            // 
            // button51
            // 
            this.button51.Location = new System.Drawing.Point(3, 228);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(39, 39);
            this.button51.TabIndex = 50;
            this.button51.Text = "button51";
            this.button51.UseVisualStyleBackColor = true;
            this.button51.Click += new System.EventHandler(this.button51_Click);
            // 
            // button52
            // 
            this.button52.Location = new System.Drawing.Point(48, 228);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(39, 39);
            this.button52.TabIndex = 51;
            this.button52.Text = "button52";
            this.button52.UseVisualStyleBackColor = true;
            this.button52.Click += new System.EventHandler(this.button52_Click);
            // 
            // button53
            // 
            this.button53.Location = new System.Drawing.Point(93, 228);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(39, 39);
            this.button53.TabIndex = 52;
            this.button53.Text = "button53";
            this.button53.UseVisualStyleBackColor = true;
            this.button53.Click += new System.EventHandler(this.button53_Click);
            // 
            // button54
            // 
            this.button54.Location = new System.Drawing.Point(138, 228);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(39, 39);
            this.button54.TabIndex = 53;
            this.button54.Text = "button54";
            this.button54.UseVisualStyleBackColor = true;
            this.button54.Click += new System.EventHandler(this.button54_Click);
            // 
            // button55
            // 
            this.button55.Location = new System.Drawing.Point(183, 228);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(39, 39);
            this.button55.TabIndex = 54;
            this.button55.Text = "button55";
            this.button55.UseVisualStyleBackColor = true;
            this.button55.Click += new System.EventHandler(this.button55_Click);
            // 
            // button56
            // 
            this.button56.Location = new System.Drawing.Point(228, 228);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(39, 39);
            this.button56.TabIndex = 55;
            this.button56.Text = "button56";
            this.button56.UseVisualStyleBackColor = true;
            this.button56.Click += new System.EventHandler(this.button56_Click);
            // 
            // button57
            // 
            this.button57.Location = new System.Drawing.Point(273, 228);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(39, 39);
            this.button57.TabIndex = 56;
            this.button57.Text = "button57";
            this.button57.UseVisualStyleBackColor = true;
            this.button57.Click += new System.EventHandler(this.button57_Click);
            // 
            // button58
            // 
            this.button58.Location = new System.Drawing.Point(318, 228);
            this.button58.Name = "button58";
            this.button58.Size = new System.Drawing.Size(39, 39);
            this.button58.TabIndex = 57;
            this.button58.Text = "button58";
            this.button58.UseVisualStyleBackColor = true;
            this.button58.Click += new System.EventHandler(this.button58_Click);
            // 
            // button59
            // 
            this.button59.Location = new System.Drawing.Point(363, 228);
            this.button59.Name = "button59";
            this.button59.Size = new System.Drawing.Size(39, 39);
            this.button59.TabIndex = 58;
            this.button59.Text = "button59";
            this.button59.UseVisualStyleBackColor = true;
            this.button59.Click += new System.EventHandler(this.button59_Click);
            // 
            // button60
            // 
            this.button60.Location = new System.Drawing.Point(408, 228);
            this.button60.Name = "button60";
            this.button60.Size = new System.Drawing.Size(39, 39);
            this.button60.TabIndex = 59;
            this.button60.Text = "button60";
            this.button60.UseVisualStyleBackColor = true;
            this.button60.Click += new System.EventHandler(this.button60_Click);
            // 
            // button61
            // 
            this.button61.Location = new System.Drawing.Point(3, 273);
            this.button61.Name = "button61";
            this.button61.Size = new System.Drawing.Size(39, 39);
            this.button61.TabIndex = 60;
            this.button61.Text = "button61";
            this.button61.UseVisualStyleBackColor = true;
            this.button61.Click += new System.EventHandler(this.button61_Click);
            // 
            // button62
            // 
            this.button62.Location = new System.Drawing.Point(48, 273);
            this.button62.Name = "button62";
            this.button62.Size = new System.Drawing.Size(39, 39);
            this.button62.TabIndex = 61;
            this.button62.Text = "button62";
            this.button62.UseVisualStyleBackColor = true;
            this.button62.Click += new System.EventHandler(this.button62_Click);
            // 
            // button63
            // 
            this.button63.Location = new System.Drawing.Point(93, 273);
            this.button63.Name = "button63";
            this.button63.Size = new System.Drawing.Size(39, 39);
            this.button63.TabIndex = 62;
            this.button63.Text = "button63";
            this.button63.UseVisualStyleBackColor = true;
            this.button63.Click += new System.EventHandler(this.button63_Click);
            // 
            // button64
            // 
            this.button64.Location = new System.Drawing.Point(138, 273);
            this.button64.Name = "button64";
            this.button64.Size = new System.Drawing.Size(39, 39);
            this.button64.TabIndex = 63;
            this.button64.Text = "button64";
            this.button64.UseVisualStyleBackColor = true;
            this.button64.Click += new System.EventHandler(this.button64_Click);
            // 
            // button65
            // 
            this.button65.Location = new System.Drawing.Point(183, 273);
            this.button65.Name = "button65";
            this.button65.Size = new System.Drawing.Size(39, 39);
            this.button65.TabIndex = 64;
            this.button65.Text = "button65";
            this.button65.UseVisualStyleBackColor = true;
            this.button65.Click += new System.EventHandler(this.button65_Click);
            // 
            // button66
            // 
            this.button66.Location = new System.Drawing.Point(228, 273);
            this.button66.Name = "button66";
            this.button66.Size = new System.Drawing.Size(39, 39);
            this.button66.TabIndex = 65;
            this.button66.Text = "button66";
            this.button66.UseVisualStyleBackColor = true;
            this.button66.Click += new System.EventHandler(this.button66_Click);
            // 
            // button67
            // 
            this.button67.Location = new System.Drawing.Point(273, 273);
            this.button67.Name = "button67";
            this.button67.Size = new System.Drawing.Size(39, 39);
            this.button67.TabIndex = 66;
            this.button67.Text = "button67";
            this.button67.UseVisualStyleBackColor = true;
            this.button67.Click += new System.EventHandler(this.button67_Click);
            // 
            // button68
            // 
            this.button68.Location = new System.Drawing.Point(318, 273);
            this.button68.Name = "button68";
            this.button68.Size = new System.Drawing.Size(39, 39);
            this.button68.TabIndex = 67;
            this.button68.Text = "button68";
            this.button68.UseVisualStyleBackColor = true;
            this.button68.Click += new System.EventHandler(this.button68_Click);
            // 
            // button69
            // 
            this.button69.Location = new System.Drawing.Point(363, 273);
            this.button69.Name = "button69";
            this.button69.Size = new System.Drawing.Size(39, 39);
            this.button69.TabIndex = 68;
            this.button69.Text = "button69";
            this.button69.UseVisualStyleBackColor = true;
            this.button69.Click += new System.EventHandler(this.button69_Click);
            // 
            // button70
            // 
            this.button70.Location = new System.Drawing.Point(408, 273);
            this.button70.Name = "button70";
            this.button70.Size = new System.Drawing.Size(39, 39);
            this.button70.TabIndex = 69;
            this.button70.Text = "button70";
            this.button70.UseVisualStyleBackColor = true;
            this.button70.Click += new System.EventHandler(this.button70_Click);
            // 
            // button71
            // 
            this.button71.Location = new System.Drawing.Point(3, 318);
            this.button71.Name = "button71";
            this.button71.Size = new System.Drawing.Size(39, 39);
            this.button71.TabIndex = 70;
            this.button71.Text = "button71";
            this.button71.UseVisualStyleBackColor = true;
            this.button71.Click += new System.EventHandler(this.button71_Click);
            // 
            // button72
            // 
            this.button72.Location = new System.Drawing.Point(48, 318);
            this.button72.Name = "button72";
            this.button72.Size = new System.Drawing.Size(39, 39);
            this.button72.TabIndex = 71;
            this.button72.Text = "button72";
            this.button72.UseVisualStyleBackColor = true;
            this.button72.Click += new System.EventHandler(this.button72_Click);
            // 
            // button73
            // 
            this.button73.Location = new System.Drawing.Point(93, 318);
            this.button73.Name = "button73";
            this.button73.Size = new System.Drawing.Size(39, 39);
            this.button73.TabIndex = 72;
            this.button73.Text = "button73";
            this.button73.UseVisualStyleBackColor = true;
            this.button73.Click += new System.EventHandler(this.button73_Click);
            // 
            // button74
            // 
            this.button74.Location = new System.Drawing.Point(138, 318);
            this.button74.Name = "button74";
            this.button74.Size = new System.Drawing.Size(39, 39);
            this.button74.TabIndex = 73;
            this.button74.Text = "button74";
            this.button74.UseVisualStyleBackColor = true;
            this.button74.Click += new System.EventHandler(this.button74_Click);
            // 
            // button75
            // 
            this.button75.Location = new System.Drawing.Point(183, 318);
            this.button75.Name = "button75";
            this.button75.Size = new System.Drawing.Size(39, 39);
            this.button75.TabIndex = 74;
            this.button75.Text = "button75";
            this.button75.UseVisualStyleBackColor = true;
            this.button75.Click += new System.EventHandler(this.button75_Click);
            // 
            // button76
            // 
            this.button76.Location = new System.Drawing.Point(228, 318);
            this.button76.Name = "button76";
            this.button76.Size = new System.Drawing.Size(39, 39);
            this.button76.TabIndex = 75;
            this.button76.Text = "button76";
            this.button76.UseVisualStyleBackColor = true;
            this.button76.Click += new System.EventHandler(this.button76_Click);
            // 
            // button77
            // 
            this.button77.Location = new System.Drawing.Point(273, 318);
            this.button77.Name = "button77";
            this.button77.Size = new System.Drawing.Size(39, 39);
            this.button77.TabIndex = 76;
            this.button77.Text = "button77";
            this.button77.UseVisualStyleBackColor = true;
            this.button77.Click += new System.EventHandler(this.button77_Click);
            // 
            // button78
            // 
            this.button78.Location = new System.Drawing.Point(318, 318);
            this.button78.Name = "button78";
            this.button78.Size = new System.Drawing.Size(39, 39);
            this.button78.TabIndex = 77;
            this.button78.Text = "button78";
            this.button78.UseVisualStyleBackColor = true;
            this.button78.Click += new System.EventHandler(this.button78_Click);
            // 
            // button79
            // 
            this.button79.Location = new System.Drawing.Point(363, 318);
            this.button79.Name = "button79";
            this.button79.Size = new System.Drawing.Size(39, 39);
            this.button79.TabIndex = 78;
            this.button79.Text = "button79";
            this.button79.UseVisualStyleBackColor = true;
            this.button79.Click += new System.EventHandler(this.button79_Click);
            // 
            // button80
            // 
            this.button80.Location = new System.Drawing.Point(408, 318);
            this.button80.Name = "button80";
            this.button80.Size = new System.Drawing.Size(39, 39);
            this.button80.TabIndex = 79;
            this.button80.Text = "button80";
            this.button80.UseVisualStyleBackColor = true;
            this.button80.Click += new System.EventHandler(this.button80_Click);
            // 
            // button81
            // 
            this.button81.Location = new System.Drawing.Point(3, 363);
            this.button81.Name = "button81";
            this.button81.Size = new System.Drawing.Size(39, 39);
            this.button81.TabIndex = 80;
            this.button81.Text = "button81";
            this.button81.UseVisualStyleBackColor = true;
            this.button81.Click += new System.EventHandler(this.button81_Click);
            // 
            // button82
            // 
            this.button82.Location = new System.Drawing.Point(48, 363);
            this.button82.Name = "button82";
            this.button82.Size = new System.Drawing.Size(39, 39);
            this.button82.TabIndex = 81;
            this.button82.Text = "button82";
            this.button82.UseVisualStyleBackColor = true;
            this.button82.Click += new System.EventHandler(this.button82_Click);
            // 
            // button83
            // 
            this.button83.Location = new System.Drawing.Point(93, 363);
            this.button83.Name = "button83";
            this.button83.Size = new System.Drawing.Size(39, 39);
            this.button83.TabIndex = 82;
            this.button83.Text = "button83";
            this.button83.UseVisualStyleBackColor = true;
            this.button83.Click += new System.EventHandler(this.button83_Click);
            // 
            // button84
            // 
            this.button84.Location = new System.Drawing.Point(138, 363);
            this.button84.Name = "button84";
            this.button84.Size = new System.Drawing.Size(39, 39);
            this.button84.TabIndex = 83;
            this.button84.Text = "button84";
            this.button84.UseVisualStyleBackColor = true;
            this.button84.Click += new System.EventHandler(this.button84_Click);
            // 
            // button85
            // 
            this.button85.Location = new System.Drawing.Point(183, 363);
            this.button85.Name = "button85";
            this.button85.Size = new System.Drawing.Size(39, 39);
            this.button85.TabIndex = 84;
            this.button85.Text = "button85";
            this.button85.UseVisualStyleBackColor = true;
            this.button85.Click += new System.EventHandler(this.button85_Click);
            // 
            // button86
            // 
            this.button86.Location = new System.Drawing.Point(228, 363);
            this.button86.Name = "button86";
            this.button86.Size = new System.Drawing.Size(39, 39);
            this.button86.TabIndex = 85;
            this.button86.Text = "button86";
            this.button86.UseVisualStyleBackColor = true;
            this.button86.Click += new System.EventHandler(this.button86_Click);
            // 
            // button87
            // 
            this.button87.Location = new System.Drawing.Point(273, 363);
            this.button87.Name = "button87";
            this.button87.Size = new System.Drawing.Size(39, 39);
            this.button87.TabIndex = 86;
            this.button87.Text = "button87";
            this.button87.UseVisualStyleBackColor = true;
            this.button87.Click += new System.EventHandler(this.button87_Click);
            // 
            // button88
            // 
            this.button88.Location = new System.Drawing.Point(318, 363);
            this.button88.Name = "button88";
            this.button88.Size = new System.Drawing.Size(39, 39);
            this.button88.TabIndex = 87;
            this.button88.Text = "button88";
            this.button88.UseVisualStyleBackColor = true;
            this.button88.Click += new System.EventHandler(this.button88_Click);
            // 
            // button89
            // 
            this.button89.Location = new System.Drawing.Point(363, 363);
            this.button89.Name = "button89";
            this.button89.Size = new System.Drawing.Size(39, 39);
            this.button89.TabIndex = 88;
            this.button89.Text = "button89";
            this.button89.UseVisualStyleBackColor = true;
            this.button89.Click += new System.EventHandler(this.button89_Click);
            // 
            // button90
            // 
            this.button90.Location = new System.Drawing.Point(408, 363);
            this.button90.Name = "button90";
            this.button90.Size = new System.Drawing.Size(39, 39);
            this.button90.TabIndex = 89;
            this.button90.Text = "button90";
            this.button90.UseVisualStyleBackColor = true;
            this.button90.Click += new System.EventHandler(this.button90_Click);
            // 
            // button92
            // 
            this.button92.Location = new System.Drawing.Point(48, 408);
            this.button92.Name = "button92";
            this.button92.Size = new System.Drawing.Size(39, 39);
            this.button92.TabIndex = 91;
            this.button92.Text = "button92";
            this.button92.UseVisualStyleBackColor = true;
            this.button92.Click += new System.EventHandler(this.button92_Click);
            // 
            // button93
            // 
            this.button93.Location = new System.Drawing.Point(93, 408);
            this.button93.Name = "button93";
            this.button93.Size = new System.Drawing.Size(39, 39);
            this.button93.TabIndex = 92;
            this.button93.Text = "button93";
            this.button93.UseVisualStyleBackColor = true;
            this.button93.Click += new System.EventHandler(this.button93_Click);
            // 
            // button94
            // 
            this.button94.Location = new System.Drawing.Point(138, 408);
            this.button94.Name = "button94";
            this.button94.Size = new System.Drawing.Size(39, 39);
            this.button94.TabIndex = 93;
            this.button94.Text = "button94";
            this.button94.UseVisualStyleBackColor = true;
            this.button94.Click += new System.EventHandler(this.button94_Click);
            // 
            // button95
            // 
            this.button95.Location = new System.Drawing.Point(183, 408);
            this.button95.Name = "button95";
            this.button95.Size = new System.Drawing.Size(39, 39);
            this.button95.TabIndex = 94;
            this.button95.Text = "button95";
            this.button95.UseVisualStyleBackColor = true;
            this.button95.Click += new System.EventHandler(this.button95_Click);
            // 
            // button96
            // 
            this.button96.Location = new System.Drawing.Point(228, 408);
            this.button96.Name = "button96";
            this.button96.Size = new System.Drawing.Size(39, 39);
            this.button96.TabIndex = 95;
            this.button96.Text = "button96";
            this.button96.UseVisualStyleBackColor = true;
            this.button96.Click += new System.EventHandler(this.button96_Click);
            // 
            // button97
            // 
            this.button97.Location = new System.Drawing.Point(273, 408);
            this.button97.Name = "button97";
            this.button97.Size = new System.Drawing.Size(39, 39);
            this.button97.TabIndex = 96;
            this.button97.Text = "button97";
            this.button97.UseVisualStyleBackColor = true;
            this.button97.Click += new System.EventHandler(this.button97_Click);
            // 
            // button98
            // 
            this.button98.Location = new System.Drawing.Point(318, 408);
            this.button98.Name = "button98";
            this.button98.Size = new System.Drawing.Size(39, 39);
            this.button98.TabIndex = 97;
            this.button98.Text = "button98";
            this.button98.UseVisualStyleBackColor = true;
            this.button98.Click += new System.EventHandler(this.button98_Click);
            // 
            // button99
            // 
            this.button99.Location = new System.Drawing.Point(363, 408);
            this.button99.Name = "button99";
            this.button99.Size = new System.Drawing.Size(39, 39);
            this.button99.TabIndex = 98;
            this.button99.Text = "button99";
            this.button99.UseVisualStyleBackColor = true;
            this.button99.Click += new System.EventHandler(this.button99_Click);
            // 
            // button100
            // 
            this.button100.Location = new System.Drawing.Point(408, 408);
            this.button100.Name = "button100";
            this.button100.Size = new System.Drawing.Size(39, 39);
            this.button100.TabIndex = 99;
            this.button100.Text = "button100";
            this.button100.UseVisualStyleBackColor = true;
            this.button100.Click += new System.EventHandler(this.button100_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 10;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel2.Controls.Add(this.button101, 0, 9);
            this.tableLayoutPanel2.Controls.Add(this.button102, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.button103, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.button104, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.button105, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.button106, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.button107, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.button108, 6, 0);
            this.tableLayoutPanel2.Controls.Add(this.button109, 7, 0);
            this.tableLayoutPanel2.Controls.Add(this.button110, 8, 0);
            this.tableLayoutPanel2.Controls.Add(this.button111, 9, 0);
            this.tableLayoutPanel2.Controls.Add(this.button112, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.button113, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.button114, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.button115, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.button116, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.button117, 5, 1);
            this.tableLayoutPanel2.Controls.Add(this.button118, 6, 1);
            this.tableLayoutPanel2.Controls.Add(this.button119, 7, 1);
            this.tableLayoutPanel2.Controls.Add(this.button120, 8, 1);
            this.tableLayoutPanel2.Controls.Add(this.button121, 9, 1);
            this.tableLayoutPanel2.Controls.Add(this.button122, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.button123, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.button124, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.button125, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.button126, 4, 2);
            this.tableLayoutPanel2.Controls.Add(this.button127, 5, 2);
            this.tableLayoutPanel2.Controls.Add(this.button128, 6, 2);
            this.tableLayoutPanel2.Controls.Add(this.button129, 7, 2);
            this.tableLayoutPanel2.Controls.Add(this.button130, 8, 2);
            this.tableLayoutPanel2.Controls.Add(this.button131, 9, 2);
            this.tableLayoutPanel2.Controls.Add(this.button132, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.button133, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.button134, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.button135, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.button136, 4, 3);
            this.tableLayoutPanel2.Controls.Add(this.button137, 5, 3);
            this.tableLayoutPanel2.Controls.Add(this.button138, 6, 3);
            this.tableLayoutPanel2.Controls.Add(this.button139, 7, 3);
            this.tableLayoutPanel2.Controls.Add(this.button140, 8, 3);
            this.tableLayoutPanel2.Controls.Add(this.button141, 9, 3);
            this.tableLayoutPanel2.Controls.Add(this.button142, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.button143, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.button144, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.button145, 3, 4);
            this.tableLayoutPanel2.Controls.Add(this.button146, 4, 4);
            this.tableLayoutPanel2.Controls.Add(this.button147, 5, 4);
            this.tableLayoutPanel2.Controls.Add(this.button148, 6, 4);
            this.tableLayoutPanel2.Controls.Add(this.button149, 7, 4);
            this.tableLayoutPanel2.Controls.Add(this.button150, 8, 4);
            this.tableLayoutPanel2.Controls.Add(this.button151, 9, 4);
            this.tableLayoutPanel2.Controls.Add(this.button152, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.button153, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.button154, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.button155, 3, 5);
            this.tableLayoutPanel2.Controls.Add(this.button156, 4, 5);
            this.tableLayoutPanel2.Controls.Add(this.button157, 5, 5);
            this.tableLayoutPanel2.Controls.Add(this.button158, 6, 5);
            this.tableLayoutPanel2.Controls.Add(this.button159, 7, 5);
            this.tableLayoutPanel2.Controls.Add(this.button160, 8, 5);
            this.tableLayoutPanel2.Controls.Add(this.button161, 9, 5);
            this.tableLayoutPanel2.Controls.Add(this.button162, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.button163, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.button164, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.button165, 3, 6);
            this.tableLayoutPanel2.Controls.Add(this.button166, 4, 6);
            this.tableLayoutPanel2.Controls.Add(this.button167, 5, 6);
            this.tableLayoutPanel2.Controls.Add(this.button168, 6, 6);
            this.tableLayoutPanel2.Controls.Add(this.button169, 7, 6);
            this.tableLayoutPanel2.Controls.Add(this.button170, 8, 6);
            this.tableLayoutPanel2.Controls.Add(this.button171, 9, 6);
            this.tableLayoutPanel2.Controls.Add(this.button172, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.button173, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.button174, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.button175, 3, 7);
            this.tableLayoutPanel2.Controls.Add(this.button176, 4, 7);
            this.tableLayoutPanel2.Controls.Add(this.button177, 5, 7);
            this.tableLayoutPanel2.Controls.Add(this.button178, 6, 7);
            this.tableLayoutPanel2.Controls.Add(this.button179, 7, 7);
            this.tableLayoutPanel2.Controls.Add(this.button180, 8, 7);
            this.tableLayoutPanel2.Controls.Add(this.button181, 9, 7);
            this.tableLayoutPanel2.Controls.Add(this.button182, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.button183, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.button184, 2, 8);
            this.tableLayoutPanel2.Controls.Add(this.button185, 3, 8);
            this.tableLayoutPanel2.Controls.Add(this.button186, 4, 8);
            this.tableLayoutPanel2.Controls.Add(this.button187, 5, 8);
            this.tableLayoutPanel2.Controls.Add(this.button188, 6, 8);
            this.tableLayoutPanel2.Controls.Add(this.button189, 7, 8);
            this.tableLayoutPanel2.Controls.Add(this.button190, 8, 8);
            this.tableLayoutPanel2.Controls.Add(this.button191, 9, 8);
            this.tableLayoutPanel2.Controls.Add(this.button192, 1, 9);
            this.tableLayoutPanel2.Controls.Add(this.button193, 2, 9);
            this.tableLayoutPanel2.Controls.Add(this.button194, 3, 9);
            this.tableLayoutPanel2.Controls.Add(this.button195, 4, 9);
            this.tableLayoutPanel2.Controls.Add(this.button196, 5, 9);
            this.tableLayoutPanel2.Controls.Add(this.button197, 6, 9);
            this.tableLayoutPanel2.Controls.Add(this.button198, 7, 9);
            this.tableLayoutPanel2.Controls.Add(this.button199, 8, 9);
            this.tableLayoutPanel2.Controls.Add(this.button200, 9, 9);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(810, 35);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 10;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(452, 452);
            this.tableLayoutPanel2.TabIndex = 100;
            // 
            // button101
            // 
            this.button101.Location = new System.Drawing.Point(3, 408);
            this.button101.Name = "button101";
            this.button101.Size = new System.Drawing.Size(39, 39);
            this.button101.TabIndex = 90;
            this.button101.Text = "button101";
            this.button101.UseVisualStyleBackColor = true;
            this.button101.Click += new System.EventHandler(this.button101_Click);
            // 
            // button102
            // 
            this.button102.Location = new System.Drawing.Point(48, 3);
            this.button102.Name = "button102";
            this.button102.Size = new System.Drawing.Size(39, 38);
            this.button102.TabIndex = 1;
            this.button102.Text = "button102";
            this.button102.UseVisualStyleBackColor = true;
            this.button102.Click += new System.EventHandler(this.button102_Click);
            // 
            // button103
            // 
            this.button103.Location = new System.Drawing.Point(3, 3);
            this.button103.Name = "button103";
            this.button103.Size = new System.Drawing.Size(39, 38);
            this.button103.TabIndex = 0;
            this.button103.Text = "button103";
            this.button103.UseVisualStyleBackColor = true;
            this.button103.Click += new System.EventHandler(this.button103_Click);
            // 
            // button104
            // 
            this.button104.Location = new System.Drawing.Point(93, 3);
            this.button104.Name = "button104";
            this.button104.Size = new System.Drawing.Size(39, 38);
            this.button104.TabIndex = 2;
            this.button104.Text = "button104";
            this.button104.UseVisualStyleBackColor = true;
            this.button104.Click += new System.EventHandler(this.button104_Click);
            // 
            // button105
            // 
            this.button105.Location = new System.Drawing.Point(138, 3);
            this.button105.Name = "button105";
            this.button105.Size = new System.Drawing.Size(39, 38);
            this.button105.TabIndex = 3;
            this.button105.Text = "button105";
            this.button105.UseVisualStyleBackColor = true;
            this.button105.Click += new System.EventHandler(this.button105_Click);
            // 
            // button106
            // 
            this.button106.Location = new System.Drawing.Point(183, 3);
            this.button106.Name = "button106";
            this.button106.Size = new System.Drawing.Size(39, 38);
            this.button106.TabIndex = 4;
            this.button106.Text = "button106";
            this.button106.UseVisualStyleBackColor = true;
            this.button106.Click += new System.EventHandler(this.button106_Click);
            // 
            // button107
            // 
            this.button107.Location = new System.Drawing.Point(228, 3);
            this.button107.Name = "button107";
            this.button107.Size = new System.Drawing.Size(39, 38);
            this.button107.TabIndex = 5;
            this.button107.Text = "button107";
            this.button107.UseVisualStyleBackColor = true;
            this.button107.Click += new System.EventHandler(this.button107_Click);
            // 
            // button108
            // 
            this.button108.Location = new System.Drawing.Point(273, 3);
            this.button108.Name = "button108";
            this.button108.Size = new System.Drawing.Size(39, 38);
            this.button108.TabIndex = 6;
            this.button108.Text = "button108";
            this.button108.UseVisualStyleBackColor = true;
            this.button108.Click += new System.EventHandler(this.button108_Click);
            // 
            // button109
            // 
            this.button109.Location = new System.Drawing.Point(318, 3);
            this.button109.Name = "button109";
            this.button109.Size = new System.Drawing.Size(39, 38);
            this.button109.TabIndex = 7;
            this.button109.Text = "button109";
            this.button109.UseVisualStyleBackColor = true;
            this.button109.Click += new System.EventHandler(this.button109_Click);
            // 
            // button110
            // 
            this.button110.Location = new System.Drawing.Point(363, 3);
            this.button110.Name = "button110";
            this.button110.Size = new System.Drawing.Size(39, 38);
            this.button110.TabIndex = 8;
            this.button110.Text = "button110";
            this.button110.UseVisualStyleBackColor = true;
            this.button110.Click += new System.EventHandler(this.button110_Click);
            // 
            // button111
            // 
            this.button111.Location = new System.Drawing.Point(408, 3);
            this.button111.Name = "button111";
            this.button111.Size = new System.Drawing.Size(39, 38);
            this.button111.TabIndex = 9;
            this.button111.Text = "button111";
            this.button111.UseVisualStyleBackColor = true;
            this.button111.Click += new System.EventHandler(this.button111_Click);
            // 
            // button112
            // 
            this.button112.Location = new System.Drawing.Point(3, 47);
            this.button112.Name = "button112";
            this.button112.Size = new System.Drawing.Size(39, 39);
            this.button112.TabIndex = 10;
            this.button112.Text = "button112";
            this.button112.UseVisualStyleBackColor = true;
            this.button112.Click += new System.EventHandler(this.button112_Click);
            // 
            // button113
            // 
            this.button113.Location = new System.Drawing.Point(48, 47);
            this.button113.Name = "button113";
            this.button113.Size = new System.Drawing.Size(39, 39);
            this.button113.TabIndex = 11;
            this.button113.Text = "button113";
            this.button113.UseVisualStyleBackColor = true;
            this.button113.Click += new System.EventHandler(this.button113_Click);
            // 
            // button114
            // 
            this.button114.Location = new System.Drawing.Point(93, 47);
            this.button114.Name = "button114";
            this.button114.Size = new System.Drawing.Size(39, 39);
            this.button114.TabIndex = 12;
            this.button114.Text = "button114";
            this.button114.UseVisualStyleBackColor = true;
            this.button114.Click += new System.EventHandler(this.button114_Click);
            // 
            // button115
            // 
            this.button115.Location = new System.Drawing.Point(138, 47);
            this.button115.Name = "button115";
            this.button115.Size = new System.Drawing.Size(39, 39);
            this.button115.TabIndex = 13;
            this.button115.Text = "button115";
            this.button115.UseVisualStyleBackColor = true;
            this.button115.Click += new System.EventHandler(this.button115_Click);
            // 
            // button116
            // 
            this.button116.Location = new System.Drawing.Point(183, 47);
            this.button116.Name = "button116";
            this.button116.Size = new System.Drawing.Size(39, 39);
            this.button116.TabIndex = 14;
            this.button116.Text = "button116";
            this.button116.UseVisualStyleBackColor = true;
            this.button116.Click += new System.EventHandler(this.button116_Click);
            // 
            // button117
            // 
            this.button117.Location = new System.Drawing.Point(228, 47);
            this.button117.Name = "button117";
            this.button117.Size = new System.Drawing.Size(39, 39);
            this.button117.TabIndex = 15;
            this.button117.Text = "button117";
            this.button117.UseVisualStyleBackColor = true;
            this.button117.Click += new System.EventHandler(this.button117_Click);
            // 
            // button118
            // 
            this.button118.Location = new System.Drawing.Point(273, 47);
            this.button118.Name = "button118";
            this.button118.Size = new System.Drawing.Size(39, 39);
            this.button118.TabIndex = 16;
            this.button118.Text = "button118";
            this.button118.UseVisualStyleBackColor = true;
            this.button118.Click += new System.EventHandler(this.button118_Click);
            // 
            // button119
            // 
            this.button119.Location = new System.Drawing.Point(318, 47);
            this.button119.Name = "button119";
            this.button119.Size = new System.Drawing.Size(39, 39);
            this.button119.TabIndex = 17;
            this.button119.Text = "button119";
            this.button119.UseVisualStyleBackColor = true;
            this.button119.Click += new System.EventHandler(this.button119_Click);
            // 
            // button120
            // 
            this.button120.Location = new System.Drawing.Point(363, 47);
            this.button120.Name = "button120";
            this.button120.Size = new System.Drawing.Size(39, 39);
            this.button120.TabIndex = 18;
            this.button120.Text = "button120";
            this.button120.UseVisualStyleBackColor = true;
            this.button120.Click += new System.EventHandler(this.button120_Click);
            // 
            // button121
            // 
            this.button121.Location = new System.Drawing.Point(408, 47);
            this.button121.Name = "button121";
            this.button121.Size = new System.Drawing.Size(39, 39);
            this.button121.TabIndex = 19;
            this.button121.Text = "button121";
            this.button121.UseVisualStyleBackColor = true;
            this.button121.Click += new System.EventHandler(this.button121_Click);
            // 
            // button122
            // 
            this.button122.Location = new System.Drawing.Point(3, 93);
            this.button122.Name = "button122";
            this.button122.Size = new System.Drawing.Size(39, 39);
            this.button122.TabIndex = 20;
            this.button122.Text = "button122";
            this.button122.UseVisualStyleBackColor = true;
            this.button122.Click += new System.EventHandler(this.button122_Click);
            // 
            // button123
            // 
            this.button123.Location = new System.Drawing.Point(48, 93);
            this.button123.Name = "button123";
            this.button123.Size = new System.Drawing.Size(39, 39);
            this.button123.TabIndex = 21;
            this.button123.Text = "button123";
            this.button123.UseVisualStyleBackColor = true;
            this.button123.Click += new System.EventHandler(this.button123_Click);
            // 
            // button124
            // 
            this.button124.Location = new System.Drawing.Point(93, 93);
            this.button124.Name = "button124";
            this.button124.Size = new System.Drawing.Size(39, 39);
            this.button124.TabIndex = 22;
            this.button124.Text = "button124";
            this.button124.UseVisualStyleBackColor = true;
            this.button124.Click += new System.EventHandler(this.button124_Click);
            // 
            // button125
            // 
            this.button125.Location = new System.Drawing.Point(138, 93);
            this.button125.Name = "button125";
            this.button125.Size = new System.Drawing.Size(39, 39);
            this.button125.TabIndex = 23;
            this.button125.Text = "button125";
            this.button125.UseVisualStyleBackColor = true;
            this.button125.Click += new System.EventHandler(this.button125_Click);
            // 
            // button126
            // 
            this.button126.Location = new System.Drawing.Point(183, 93);
            this.button126.Name = "button126";
            this.button126.Size = new System.Drawing.Size(39, 39);
            this.button126.TabIndex = 24;
            this.button126.Text = "button126";
            this.button126.UseVisualStyleBackColor = true;
            this.button126.Click += new System.EventHandler(this.button126_Click);
            // 
            // button127
            // 
            this.button127.Location = new System.Drawing.Point(228, 93);
            this.button127.Name = "button127";
            this.button127.Size = new System.Drawing.Size(39, 39);
            this.button127.TabIndex = 25;
            this.button127.Text = "button127";
            this.button127.UseVisualStyleBackColor = true;
            this.button127.Click += new System.EventHandler(this.button127_Click);
            // 
            // button128
            // 
            this.button128.Location = new System.Drawing.Point(273, 93);
            this.button128.Name = "button128";
            this.button128.Size = new System.Drawing.Size(39, 39);
            this.button128.TabIndex = 26;
            this.button128.Text = "button128";
            this.button128.UseVisualStyleBackColor = true;
            this.button128.Click += new System.EventHandler(this.button128_Click);
            // 
            // button129
            // 
            this.button129.Location = new System.Drawing.Point(318, 93);
            this.button129.Name = "button129";
            this.button129.Size = new System.Drawing.Size(39, 39);
            this.button129.TabIndex = 27;
            this.button129.Text = "button129";
            this.button129.UseVisualStyleBackColor = true;
            this.button129.Click += new System.EventHandler(this.button129_Click);
            // 
            // button130
            // 
            this.button130.Location = new System.Drawing.Point(363, 93);
            this.button130.Name = "button130";
            this.button130.Size = new System.Drawing.Size(39, 39);
            this.button130.TabIndex = 28;
            this.button130.Text = "button130";
            this.button130.UseVisualStyleBackColor = true;
            this.button130.Click += new System.EventHandler(this.button130_Click);
            // 
            // button131
            // 
            this.button131.Location = new System.Drawing.Point(408, 93);
            this.button131.Name = "button131";
            this.button131.Size = new System.Drawing.Size(39, 39);
            this.button131.TabIndex = 29;
            this.button131.Text = "button131";
            this.button131.UseVisualStyleBackColor = true;
            this.button131.Click += new System.EventHandler(this.button131_Click);
            // 
            // button132
            // 
            this.button132.Location = new System.Drawing.Point(3, 138);
            this.button132.Name = "button132";
            this.button132.Size = new System.Drawing.Size(39, 39);
            this.button132.TabIndex = 30;
            this.button132.Text = "button132";
            this.button132.UseVisualStyleBackColor = true;
            this.button132.Click += new System.EventHandler(this.button132_Click);
            // 
            // button133
            // 
            this.button133.Location = new System.Drawing.Point(48, 138);
            this.button133.Name = "button133";
            this.button133.Size = new System.Drawing.Size(39, 39);
            this.button133.TabIndex = 31;
            this.button133.Text = "button133";
            this.button133.UseVisualStyleBackColor = true;
            this.button133.Click += new System.EventHandler(this.button133_Click);
            // 
            // button134
            // 
            this.button134.Location = new System.Drawing.Point(93, 138);
            this.button134.Name = "button134";
            this.button134.Size = new System.Drawing.Size(39, 39);
            this.button134.TabIndex = 32;
            this.button134.Text = "button134";
            this.button134.UseVisualStyleBackColor = true;
            this.button134.Click += new System.EventHandler(this.button134_Click);
            // 
            // button135
            // 
            this.button135.Location = new System.Drawing.Point(138, 138);
            this.button135.Name = "button135";
            this.button135.Size = new System.Drawing.Size(39, 39);
            this.button135.TabIndex = 33;
            this.button135.Text = "button135";
            this.button135.UseVisualStyleBackColor = true;
            this.button135.Click += new System.EventHandler(this.button135_Click);
            // 
            // button136
            // 
            this.button136.Location = new System.Drawing.Point(183, 138);
            this.button136.Name = "button136";
            this.button136.Size = new System.Drawing.Size(39, 39);
            this.button136.TabIndex = 34;
            this.button136.Text = "button136";
            this.button136.UseVisualStyleBackColor = true;
            this.button136.Click += new System.EventHandler(this.button136_Click);
            // 
            // button137
            // 
            this.button137.Location = new System.Drawing.Point(228, 138);
            this.button137.Name = "button137";
            this.button137.Size = new System.Drawing.Size(39, 39);
            this.button137.TabIndex = 35;
            this.button137.Text = "button137";
            this.button137.UseVisualStyleBackColor = true;
            this.button137.Click += new System.EventHandler(this.button137_Click);
            // 
            // button138
            // 
            this.button138.Location = new System.Drawing.Point(273, 138);
            this.button138.Name = "button138";
            this.button138.Size = new System.Drawing.Size(39, 39);
            this.button138.TabIndex = 36;
            this.button138.Text = "button138";
            this.button138.UseVisualStyleBackColor = true;
            this.button138.Click += new System.EventHandler(this.button138_Click);
            // 
            // button139
            // 
            this.button139.Location = new System.Drawing.Point(318, 138);
            this.button139.Name = "button139";
            this.button139.Size = new System.Drawing.Size(39, 39);
            this.button139.TabIndex = 37;
            this.button139.Text = "button139";
            this.button139.UseVisualStyleBackColor = true;
            this.button139.Click += new System.EventHandler(this.button139_Click);
            // 
            // button140
            // 
            this.button140.Location = new System.Drawing.Point(363, 138);
            this.button140.Name = "button140";
            this.button140.Size = new System.Drawing.Size(39, 39);
            this.button140.TabIndex = 38;
            this.button140.Text = "button140";
            this.button140.UseVisualStyleBackColor = true;
            this.button140.Click += new System.EventHandler(this.button140_Click);
            // 
            // button141
            // 
            this.button141.Location = new System.Drawing.Point(408, 138);
            this.button141.Name = "button141";
            this.button141.Size = new System.Drawing.Size(39, 39);
            this.button141.TabIndex = 39;
            this.button141.Text = "button141";
            this.button141.UseVisualStyleBackColor = true;
            this.button141.Click += new System.EventHandler(this.button141_Click);
            // 
            // button142
            // 
            this.button142.Location = new System.Drawing.Point(3, 183);
            this.button142.Name = "button142";
            this.button142.Size = new System.Drawing.Size(39, 39);
            this.button142.TabIndex = 40;
            this.button142.Text = "button142";
            this.button142.UseVisualStyleBackColor = true;
            this.button142.Click += new System.EventHandler(this.button142_Click);
            // 
            // button143
            // 
            this.button143.Location = new System.Drawing.Point(48, 183);
            this.button143.Name = "button143";
            this.button143.Size = new System.Drawing.Size(39, 39);
            this.button143.TabIndex = 41;
            this.button143.Text = "button143";
            this.button143.UseVisualStyleBackColor = true;
            this.button143.Click += new System.EventHandler(this.button143_Click);
            // 
            // button144
            // 
            this.button144.Location = new System.Drawing.Point(93, 183);
            this.button144.Name = "button144";
            this.button144.Size = new System.Drawing.Size(39, 39);
            this.button144.TabIndex = 42;
            this.button144.Text = "button144";
            this.button144.UseVisualStyleBackColor = true;
            this.button144.Click += new System.EventHandler(this.button144_Click);
            // 
            // button145
            // 
            this.button145.Location = new System.Drawing.Point(138, 183);
            this.button145.Name = "button145";
            this.button145.Size = new System.Drawing.Size(39, 39);
            this.button145.TabIndex = 43;
            this.button145.Text = "button145";
            this.button145.UseVisualStyleBackColor = true;
            this.button145.Click += new System.EventHandler(this.button145_Click);
            // 
            // button146
            // 
            this.button146.Location = new System.Drawing.Point(183, 183);
            this.button146.Name = "button146";
            this.button146.Size = new System.Drawing.Size(39, 39);
            this.button146.TabIndex = 44;
            this.button146.Text = "button146";
            this.button146.UseVisualStyleBackColor = true;
            this.button146.Click += new System.EventHandler(this.button146_Click);
            // 
            // button147
            // 
            this.button147.Location = new System.Drawing.Point(228, 183);
            this.button147.Name = "button147";
            this.button147.Size = new System.Drawing.Size(39, 39);
            this.button147.TabIndex = 45;
            this.button147.Text = "button147";
            this.button147.UseVisualStyleBackColor = true;
            this.button147.Click += new System.EventHandler(this.button147_Click);
            // 
            // button148
            // 
            this.button148.Location = new System.Drawing.Point(273, 183);
            this.button148.Name = "button148";
            this.button148.Size = new System.Drawing.Size(39, 39);
            this.button148.TabIndex = 46;
            this.button148.Text = "button148";
            this.button148.UseVisualStyleBackColor = true;
            this.button148.Click += new System.EventHandler(this.button148_Click);
            // 
            // button149
            // 
            this.button149.Location = new System.Drawing.Point(318, 183);
            this.button149.Name = "button149";
            this.button149.Size = new System.Drawing.Size(39, 39);
            this.button149.TabIndex = 47;
            this.button149.Text = "button149";
            this.button149.UseVisualStyleBackColor = true;
            this.button149.Click += new System.EventHandler(this.button149_Click);
            // 
            // button150
            // 
            this.button150.Location = new System.Drawing.Point(363, 183);
            this.button150.Name = "button150";
            this.button150.Size = new System.Drawing.Size(39, 39);
            this.button150.TabIndex = 48;
            this.button150.Text = "button150";
            this.button150.UseVisualStyleBackColor = true;
            this.button150.Click += new System.EventHandler(this.button150_Click);
            // 
            // button151
            // 
            this.button151.Location = new System.Drawing.Point(408, 183);
            this.button151.Name = "button151";
            this.button151.Size = new System.Drawing.Size(39, 39);
            this.button151.TabIndex = 49;
            this.button151.Text = "button151";
            this.button151.UseVisualStyleBackColor = true;
            this.button151.Click += new System.EventHandler(this.button151_Click);
            // 
            // button152
            // 
            this.button152.Location = new System.Drawing.Point(3, 228);
            this.button152.Name = "button152";
            this.button152.Size = new System.Drawing.Size(39, 39);
            this.button152.TabIndex = 50;
            this.button152.Text = "button152";
            this.button152.UseVisualStyleBackColor = true;
            this.button152.Click += new System.EventHandler(this.button152_Click);
            // 
            // button153
            // 
            this.button153.Location = new System.Drawing.Point(48, 228);
            this.button153.Name = "button153";
            this.button153.Size = new System.Drawing.Size(39, 39);
            this.button153.TabIndex = 51;
            this.button153.Text = "button153";
            this.button153.UseVisualStyleBackColor = true;
            this.button153.Click += new System.EventHandler(this.button153_Click);
            // 
            // button154
            // 
            this.button154.Location = new System.Drawing.Point(93, 228);
            this.button154.Name = "button154";
            this.button154.Size = new System.Drawing.Size(39, 39);
            this.button154.TabIndex = 52;
            this.button154.Text = "button154";
            this.button154.UseVisualStyleBackColor = true;
            this.button154.Click += new System.EventHandler(this.button154_Click);
            // 
            // button155
            // 
            this.button155.Location = new System.Drawing.Point(138, 228);
            this.button155.Name = "button155";
            this.button155.Size = new System.Drawing.Size(39, 39);
            this.button155.TabIndex = 53;
            this.button155.Text = "button155";
            this.button155.UseVisualStyleBackColor = true;
            this.button155.Click += new System.EventHandler(this.button155_Click);
            // 
            // button156
            // 
            this.button156.Location = new System.Drawing.Point(183, 228);
            this.button156.Name = "button156";
            this.button156.Size = new System.Drawing.Size(39, 39);
            this.button156.TabIndex = 54;
            this.button156.Text = "button156";
            this.button156.UseVisualStyleBackColor = true;
            this.button156.Click += new System.EventHandler(this.button156_Click);
            // 
            // button157
            // 
            this.button157.Location = new System.Drawing.Point(228, 228);
            this.button157.Name = "button157";
            this.button157.Size = new System.Drawing.Size(39, 39);
            this.button157.TabIndex = 55;
            this.button157.Text = "button157";
            this.button157.UseVisualStyleBackColor = true;
            this.button157.Click += new System.EventHandler(this.button157_Click);
            // 
            // button158
            // 
            this.button158.Location = new System.Drawing.Point(273, 228);
            this.button158.Name = "button158";
            this.button158.Size = new System.Drawing.Size(39, 39);
            this.button158.TabIndex = 56;
            this.button158.Text = "button158";
            this.button158.UseVisualStyleBackColor = true;
            this.button158.Click += new System.EventHandler(this.button158_Click);
            // 
            // button159
            // 
            this.button159.Location = new System.Drawing.Point(318, 228);
            this.button159.Name = "button159";
            this.button159.Size = new System.Drawing.Size(39, 39);
            this.button159.TabIndex = 57;
            this.button159.Text = "button159";
            this.button159.UseVisualStyleBackColor = true;
            this.button159.Click += new System.EventHandler(this.button159_Click);
            // 
            // button160
            // 
            this.button160.Location = new System.Drawing.Point(363, 228);
            this.button160.Name = "button160";
            this.button160.Size = new System.Drawing.Size(39, 39);
            this.button160.TabIndex = 58;
            this.button160.Text = "button160";
            this.button160.UseVisualStyleBackColor = true;
            this.button160.Click += new System.EventHandler(this.button160_Click);
            // 
            // button161
            // 
            this.button161.Location = new System.Drawing.Point(408, 228);
            this.button161.Name = "button161";
            this.button161.Size = new System.Drawing.Size(39, 39);
            this.button161.TabIndex = 59;
            this.button161.Text = "button161";
            this.button161.UseVisualStyleBackColor = true;
            this.button161.Click += new System.EventHandler(this.button161_Click);
            // 
            // button162
            // 
            this.button162.Location = new System.Drawing.Point(3, 273);
            this.button162.Name = "button162";
            this.button162.Size = new System.Drawing.Size(39, 39);
            this.button162.TabIndex = 60;
            this.button162.Text = "button162";
            this.button162.UseVisualStyleBackColor = true;
            this.button162.Click += new System.EventHandler(this.button162_Click);
            // 
            // button163
            // 
            this.button163.Location = new System.Drawing.Point(48, 273);
            this.button163.Name = "button163";
            this.button163.Size = new System.Drawing.Size(39, 39);
            this.button163.TabIndex = 61;
            this.button163.Text = "button163";
            this.button163.UseVisualStyleBackColor = true;
            this.button163.Click += new System.EventHandler(this.button163_Click);
            // 
            // button164
            // 
            this.button164.Location = new System.Drawing.Point(93, 273);
            this.button164.Name = "button164";
            this.button164.Size = new System.Drawing.Size(39, 39);
            this.button164.TabIndex = 62;
            this.button164.Text = "button164";
            this.button164.UseVisualStyleBackColor = true;
            this.button164.Click += new System.EventHandler(this.button164_Click);
            // 
            // button165
            // 
            this.button165.Location = new System.Drawing.Point(138, 273);
            this.button165.Name = "button165";
            this.button165.Size = new System.Drawing.Size(39, 39);
            this.button165.TabIndex = 63;
            this.button165.Text = "button165";
            this.button165.UseVisualStyleBackColor = true;
            this.button165.Click += new System.EventHandler(this.button165_Click);
            // 
            // button166
            // 
            this.button166.Location = new System.Drawing.Point(183, 273);
            this.button166.Name = "button166";
            this.button166.Size = new System.Drawing.Size(39, 39);
            this.button166.TabIndex = 64;
            this.button166.Text = "button166";
            this.button166.UseVisualStyleBackColor = true;
            this.button166.Click += new System.EventHandler(this.button166_Click);
            // 
            // button167
            // 
            this.button167.Location = new System.Drawing.Point(228, 273);
            this.button167.Name = "button167";
            this.button167.Size = new System.Drawing.Size(39, 39);
            this.button167.TabIndex = 65;
            this.button167.Text = "button167";
            this.button167.UseVisualStyleBackColor = true;
            this.button167.Click += new System.EventHandler(this.button167_Click);
            // 
            // button168
            // 
            this.button168.Location = new System.Drawing.Point(273, 273);
            this.button168.Name = "button168";
            this.button168.Size = new System.Drawing.Size(39, 39);
            this.button168.TabIndex = 66;
            this.button168.Text = "button168";
            this.button168.UseVisualStyleBackColor = true;
            this.button168.Click += new System.EventHandler(this.button168_Click);
            // 
            // button169
            // 
            this.button169.Location = new System.Drawing.Point(318, 273);
            this.button169.Name = "button169";
            this.button169.Size = new System.Drawing.Size(39, 39);
            this.button169.TabIndex = 67;
            this.button169.Text = "button169";
            this.button169.UseVisualStyleBackColor = true;
            this.button169.Click += new System.EventHandler(this.button169_Click);
            // 
            // button170
            // 
            this.button170.Location = new System.Drawing.Point(363, 273);
            this.button170.Name = "button170";
            this.button170.Size = new System.Drawing.Size(39, 39);
            this.button170.TabIndex = 68;
            this.button170.Text = "button170";
            this.button170.UseVisualStyleBackColor = true;
            this.button170.Click += new System.EventHandler(this.button170_Click);
            // 
            // button171
            // 
            this.button171.Location = new System.Drawing.Point(408, 273);
            this.button171.Name = "button171";
            this.button171.Size = new System.Drawing.Size(39, 39);
            this.button171.TabIndex = 69;
            this.button171.Text = "button171";
            this.button171.UseVisualStyleBackColor = true;
            this.button171.Click += new System.EventHandler(this.button171_Click);
            // 
            // button172
            // 
            this.button172.Location = new System.Drawing.Point(3, 318);
            this.button172.Name = "button172";
            this.button172.Size = new System.Drawing.Size(39, 39);
            this.button172.TabIndex = 70;
            this.button172.Text = "button172";
            this.button172.UseVisualStyleBackColor = true;
            this.button172.Click += new System.EventHandler(this.button172_Click);
            // 
            // button173
            // 
            this.button173.Location = new System.Drawing.Point(48, 318);
            this.button173.Name = "button173";
            this.button173.Size = new System.Drawing.Size(39, 39);
            this.button173.TabIndex = 71;
            this.button173.Text = "button173";
            this.button173.UseVisualStyleBackColor = true;
            this.button173.Click += new System.EventHandler(this.button173_Click);
            // 
            // button174
            // 
            this.button174.Location = new System.Drawing.Point(93, 318);
            this.button174.Name = "button174";
            this.button174.Size = new System.Drawing.Size(39, 39);
            this.button174.TabIndex = 72;
            this.button174.Text = "button174";
            this.button174.UseVisualStyleBackColor = true;
            this.button174.Click += new System.EventHandler(this.button174_Click);
            // 
            // button175
            // 
            this.button175.Location = new System.Drawing.Point(138, 318);
            this.button175.Name = "button175";
            this.button175.Size = new System.Drawing.Size(39, 39);
            this.button175.TabIndex = 73;
            this.button175.Text = "button175";
            this.button175.UseVisualStyleBackColor = true;
            this.button175.Click += new System.EventHandler(this.button175_Click);
            // 
            // button176
            // 
            this.button176.Location = new System.Drawing.Point(183, 318);
            this.button176.Name = "button176";
            this.button176.Size = new System.Drawing.Size(39, 39);
            this.button176.TabIndex = 74;
            this.button176.Text = "button176";
            this.button176.UseVisualStyleBackColor = true;
            this.button176.Click += new System.EventHandler(this.button176_Click);
            // 
            // button177
            // 
            this.button177.Location = new System.Drawing.Point(228, 318);
            this.button177.Name = "button177";
            this.button177.Size = new System.Drawing.Size(39, 39);
            this.button177.TabIndex = 75;
            this.button177.Text = "button177";
            this.button177.UseVisualStyleBackColor = true;
            this.button177.Click += new System.EventHandler(this.button177_Click);
            // 
            // button178
            // 
            this.button178.Location = new System.Drawing.Point(273, 318);
            this.button178.Name = "button178";
            this.button178.Size = new System.Drawing.Size(39, 39);
            this.button178.TabIndex = 76;
            this.button178.Text = "button178";
            this.button178.UseVisualStyleBackColor = true;
            this.button178.Click += new System.EventHandler(this.button178_Click);
            // 
            // button179
            // 
            this.button179.Location = new System.Drawing.Point(318, 318);
            this.button179.Name = "button179";
            this.button179.Size = new System.Drawing.Size(39, 39);
            this.button179.TabIndex = 77;
            this.button179.Text = "button179";
            this.button179.UseVisualStyleBackColor = true;
            this.button179.Click += new System.EventHandler(this.button179_Click);
            // 
            // button180
            // 
            this.button180.Location = new System.Drawing.Point(363, 318);
            this.button180.Name = "button180";
            this.button180.Size = new System.Drawing.Size(39, 39);
            this.button180.TabIndex = 78;
            this.button180.Text = "button180";
            this.button180.UseVisualStyleBackColor = true;
            this.button180.Click += new System.EventHandler(this.button180_Click);
            // 
            // button181
            // 
            this.button181.Location = new System.Drawing.Point(408, 318);
            this.button181.Name = "button181";
            this.button181.Size = new System.Drawing.Size(39, 39);
            this.button181.TabIndex = 79;
            this.button181.Text = "button181";
            this.button181.UseVisualStyleBackColor = true;
            this.button181.Click += new System.EventHandler(this.button181_Click);
            // 
            // button182
            // 
            this.button182.Location = new System.Drawing.Point(3, 363);
            this.button182.Name = "button182";
            this.button182.Size = new System.Drawing.Size(39, 39);
            this.button182.TabIndex = 80;
            this.button182.Text = "button182";
            this.button182.UseVisualStyleBackColor = true;
            this.button182.Click += new System.EventHandler(this.button182_Click);
            // 
            // button183
            // 
            this.button183.Location = new System.Drawing.Point(48, 363);
            this.button183.Name = "button183";
            this.button183.Size = new System.Drawing.Size(39, 39);
            this.button183.TabIndex = 81;
            this.button183.Text = "button183";
            this.button183.UseVisualStyleBackColor = true;
            this.button183.Click += new System.EventHandler(this.button183_Click);
            // 
            // button184
            // 
            this.button184.Location = new System.Drawing.Point(93, 363);
            this.button184.Name = "button184";
            this.button184.Size = new System.Drawing.Size(39, 39);
            this.button184.TabIndex = 82;
            this.button184.Text = "button184";
            this.button184.UseVisualStyleBackColor = true;
            this.button184.Click += new System.EventHandler(this.button184_Click);
            // 
            // button185
            // 
            this.button185.Location = new System.Drawing.Point(138, 363);
            this.button185.Name = "button185";
            this.button185.Size = new System.Drawing.Size(39, 39);
            this.button185.TabIndex = 83;
            this.button185.Text = "button185";
            this.button185.UseVisualStyleBackColor = true;
            this.button185.Click += new System.EventHandler(this.button185_Click);
            // 
            // button186
            // 
            this.button186.Location = new System.Drawing.Point(183, 363);
            this.button186.Name = "button186";
            this.button186.Size = new System.Drawing.Size(39, 39);
            this.button186.TabIndex = 84;
            this.button186.Text = "button186";
            this.button186.UseVisualStyleBackColor = true;
            this.button186.Click += new System.EventHandler(this.button186_Click);
            // 
            // button187
            // 
            this.button187.Location = new System.Drawing.Point(228, 363);
            this.button187.Name = "button187";
            this.button187.Size = new System.Drawing.Size(39, 39);
            this.button187.TabIndex = 85;
            this.button187.Text = "button187";
            this.button187.UseVisualStyleBackColor = true;
            this.button187.Click += new System.EventHandler(this.button187_Click);
            // 
            // button188
            // 
            this.button188.Location = new System.Drawing.Point(273, 363);
            this.button188.Name = "button188";
            this.button188.Size = new System.Drawing.Size(39, 39);
            this.button188.TabIndex = 86;
            this.button188.Text = "button188";
            this.button188.UseVisualStyleBackColor = true;
            this.button188.Click += new System.EventHandler(this.button188_Click);
            // 
            // button189
            // 
            this.button189.Location = new System.Drawing.Point(318, 363);
            this.button189.Name = "button189";
            this.button189.Size = new System.Drawing.Size(39, 39);
            this.button189.TabIndex = 87;
            this.button189.Text = "button189";
            this.button189.UseVisualStyleBackColor = true;
            this.button189.Click += new System.EventHandler(this.button189_Click);
            // 
            // button190
            // 
            this.button190.Location = new System.Drawing.Point(363, 363);
            this.button190.Name = "button190";
            this.button190.Size = new System.Drawing.Size(39, 39);
            this.button190.TabIndex = 88;
            this.button190.Text = "button190";
            this.button190.UseVisualStyleBackColor = true;
            this.button190.Click += new System.EventHandler(this.button190_Click);
            // 
            // button191
            // 
            this.button191.Location = new System.Drawing.Point(408, 363);
            this.button191.Name = "button191";
            this.button191.Size = new System.Drawing.Size(39, 39);
            this.button191.TabIndex = 89;
            this.button191.Text = "button191";
            this.button191.UseVisualStyleBackColor = true;
            this.button191.Click += new System.EventHandler(this.button191_Click);
            // 
            // button192
            // 
            this.button192.Location = new System.Drawing.Point(48, 408);
            this.button192.Name = "button192";
            this.button192.Size = new System.Drawing.Size(39, 39);
            this.button192.TabIndex = 91;
            this.button192.Text = "button192";
            this.button192.UseVisualStyleBackColor = true;
            this.button192.Click += new System.EventHandler(this.button192_Click);
            // 
            // button193
            // 
            this.button193.Location = new System.Drawing.Point(93, 408);
            this.button193.Name = "button193";
            this.button193.Size = new System.Drawing.Size(39, 39);
            this.button193.TabIndex = 92;
            this.button193.Text = "button193";
            this.button193.UseVisualStyleBackColor = true;
            this.button193.Click += new System.EventHandler(this.button193_Click);
            // 
            // button194
            // 
            this.button194.Location = new System.Drawing.Point(138, 408);
            this.button194.Name = "button194";
            this.button194.Size = new System.Drawing.Size(39, 39);
            this.button194.TabIndex = 93;
            this.button194.Text = "button194";
            this.button194.UseVisualStyleBackColor = true;
            this.button194.Click += new System.EventHandler(this.button194_Click);
            // 
            // button195
            // 
            this.button195.Location = new System.Drawing.Point(183, 408);
            this.button195.Name = "button195";
            this.button195.Size = new System.Drawing.Size(39, 39);
            this.button195.TabIndex = 94;
            this.button195.Text = "button195";
            this.button195.UseVisualStyleBackColor = true;
            this.button195.Click += new System.EventHandler(this.button195_Click);
            // 
            // button196
            // 
            this.button196.Location = new System.Drawing.Point(228, 408);
            this.button196.Name = "button196";
            this.button196.Size = new System.Drawing.Size(39, 39);
            this.button196.TabIndex = 95;
            this.button196.Text = "button196";
            this.button196.UseVisualStyleBackColor = true;
            this.button196.Click += new System.EventHandler(this.button196_Click);
            // 
            // button197
            // 
            this.button197.Location = new System.Drawing.Point(273, 408);
            this.button197.Name = "button197";
            this.button197.Size = new System.Drawing.Size(39, 39);
            this.button197.TabIndex = 96;
            this.button197.Text = "button197";
            this.button197.UseVisualStyleBackColor = true;
            this.button197.Click += new System.EventHandler(this.button197_Click);
            // 
            // button198
            // 
            this.button198.Location = new System.Drawing.Point(318, 408);
            this.button198.Name = "button198";
            this.button198.Size = new System.Drawing.Size(39, 39);
            this.button198.TabIndex = 97;
            this.button198.Text = "button198";
            this.button198.UseVisualStyleBackColor = true;
            this.button198.Click += new System.EventHandler(this.button198_Click);
            // 
            // button199
            // 
            this.button199.Location = new System.Drawing.Point(363, 408);
            this.button199.Name = "button199";
            this.button199.Size = new System.Drawing.Size(39, 39);
            this.button199.TabIndex = 98;
            this.button199.Text = "button199";
            this.button199.UseVisualStyleBackColor = true;
            this.button199.Click += new System.EventHandler(this.button199_Click);
            // 
            // button200
            // 
            this.button200.Location = new System.Drawing.Point(408, 408);
            this.button200.Name = "button200";
            this.button200.Size = new System.Drawing.Size(39, 39);
            this.button200.TabIndex = 99;
            this.button200.Text = "button200";
            this.button200.UseVisualStyleBackColor = true;
            this.button200.Click += new System.EventHandler(this.button200_Click);
            // 
            // actionLog
            // 
            this.actionLog.FormattingEnabled = true;
            this.actionLog.Items.AddRange(new object[] {
            "Action Log"});
            this.actionLog.Location = new System.Drawing.Point(13, 12);
            this.actionLog.Name = "actionLog";
            this.actionLog.Size = new System.Drawing.Size(294, 602);
            this.actionLog.TabIndex = 101;
            // 
            // chat
            // 
            this.chat.Enabled = false;
            this.chat.FormattingEnabled = true;
            this.chat.Items.AddRange(new object[] {
            "Chat Log"});
            this.chat.Location = new System.Drawing.Point(13, 165);
            this.chat.Name = "chat";
            this.chat.Size = new System.Drawing.Size(294, 446);
            this.chat.TabIndex = 102;
            this.chat.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(13, 617);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(231, 52);
            this.textBox1.TabIndex = 103;
            this.textBox1.Visible = false;
            // 
            // button201
            // 
            this.button201.Enabled = false;
            this.button201.Location = new System.Drawing.Point(251, 618);
            this.button201.Name = "button201";
            this.button201.Size = new System.Drawing.Size(56, 51);
            this.button201.TabIndex = 104;
            this.button201.Text = "Send";
            this.button201.UseVisualStyleBackColor = true;
            this.button201.Visible = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(776, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 37);
            this.label1.TabIndex = 105;
            this.label1.Text = "A";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(776, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 37);
            this.label2.TabIndex = 106;
            this.label2.Text = "B";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(776, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 37);
            this.label3.TabIndex = 107;
            this.label3.Text = "C";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(776, 173);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 37);
            this.label4.TabIndex = 108;
            this.label4.Text = "D";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(776, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 37);
            this.label5.TabIndex = 109;
            this.label5.Text = "E";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(776, 260);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 37);
            this.label6.TabIndex = 110;
            this.label6.Text = "F";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(776, 305);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 37);
            this.label7.TabIndex = 111;
            this.label7.Text = "G";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(776, 350);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 37);
            this.label8.TabIndex = 112;
            this.label8.Text = "H";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(776, 395);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 37);
            this.label9.TabIndex = 113;
            this.label9.Text = "I";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(776, 440);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 37);
            this.label10.TabIndex = 114;
            this.label10.Text = "J";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(324, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(32, 37);
            this.label11.TabIndex = 115;
            this.label11.Text = "1";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(369, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 37);
            this.label12.TabIndex = 116;
            this.label12.Text = "2";
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(414, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 37);
            this.label13.TabIndex = 117;
            this.label13.Text = "3";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(459, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 37);
            this.label14.TabIndex = 118;
            this.label14.Text = "4";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(504, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 37);
            this.label15.TabIndex = 119;
            this.label15.Text = "5";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(549, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 37);
            this.label16.TabIndex = 120;
            this.label16.Text = "6";
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(594, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(32, 37);
            this.label17.TabIndex = 121;
            this.label17.Text = "7";
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(639, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(32, 37);
            this.label18.TabIndex = 122;
            this.label18.Text = "8";
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(684, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 37);
            this.label19.TabIndex = 123;
            this.label19.Text = "9";
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(729, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(50, 37);
            this.label20.TabIndex = 124;
            this.label20.Text = "10";
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(1215, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(50, 37);
            this.label21.TabIndex = 134;
            this.label21.Text = "10";
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(1170, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(32, 37);
            this.label22.TabIndex = 133;
            this.label22.Text = "9";
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(1125, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(32, 37);
            this.label23.TabIndex = 132;
            this.label23.Text = "8";
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(1080, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(32, 37);
            this.label24.TabIndex = 131;
            this.label24.Text = "7";
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(1035, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(32, 37);
            this.label25.TabIndex = 130;
            this.label25.Text = "6";
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(990, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(32, 37);
            this.label26.TabIndex = 129;
            this.label26.Text = "5";
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(945, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(32, 37);
            this.label27.TabIndex = 128;
            this.label27.Text = "4";
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(900, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(32, 37);
            this.label28.TabIndex = 127;
            this.label28.Text = "3";
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(855, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(32, 37);
            this.label29.TabIndex = 126;
            this.label29.Text = "2";
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(810, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(32, 37);
            this.label30.TabIndex = 125;
            this.label30.Text = "1";
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(486, 487);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(129, 23);
            this.label31.TabIndex = 135;
            this.label31.Text = "Your Board";
            this.label31.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(938, 490);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(184, 23);
            this.label32.TabIndex = 136;
            this.label32.Text = "Opponent Board";
            this.label32.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.opScoreText);
            this.groupBox1.Controls.Add(this.label38);
            this.groupBox1.Controls.Add(this.playerScoreText);
            this.groupBox1.Controls.Add(this.label36);
            this.groupBox1.Controls.Add(this.timeText);
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(321, 524);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(267, 145);
            this.groupBox1.TabIndex = 137;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Game Statistics";
            // 
            // opScoreText
            // 
            this.opScoreText.Location = new System.Drawing.Point(183, 66);
            this.opScoreText.Name = "opScoreText";
            this.opScoreText.Size = new System.Drawing.Size(77, 23);
            this.opScoreText.TabIndex = 6;
            this.opScoreText.Text = "0";
            this.opScoreText.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(7, 64);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(186, 23);
            this.label38.TabIndex = 5;
            this.label38.Text = "Opponent Score:";
            // 
            // playerScoreText
            // 
            this.playerScoreText.Location = new System.Drawing.Point(183, 43);
            this.playerScoreText.Name = "playerScoreText";
            this.playerScoreText.Size = new System.Drawing.Size(77, 23);
            this.playerScoreText.TabIndex = 4;
            this.playerScoreText.Text = "0";
            this.playerScoreText.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label36
            // 
            this.label36.Location = new System.Drawing.Point(7, 43);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(149, 23);
            this.label36.TabIndex = 3;
            this.label36.Text = "Your Score:";
            // 
            // timeText
            // 
            this.timeText.Location = new System.Drawing.Point(199, 20);
            this.timeText.Name = "timeText";
            this.timeText.Size = new System.Drawing.Size(61, 23);
            this.timeText.TabIndex = 2;
            this.timeText.Text = "0:00";
            this.timeText.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label34
            // 
            this.label34.Location = new System.Drawing.Point(6, 20);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(187, 44);
            this.label34.TabIndex = 1;
            this.label34.Text = "Time Remaining:";
            // 
            // label33
            // 
            this.label33.Location = new System.Drawing.Point(7, 20);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(100, 23);
            this.label33.TabIndex = 0;
            this.label33.Text = "Time Remaining:";
            // 
            // secondTick
            // 
            this.secondTick.Interval = 1000;
            this.secondTick.Tick += new System.EventHandler(this.SecondTickTick);
            // 
            // label35
            // 
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(597, 544);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(655, 23);
            this.label35.TabIndex = 138;
            this.label35.Text = "Game Status";
            // 
            // button202
            // 
            this.button202.Enabled = false;
            this.button202.Location = new System.Drawing.Point(600, 588);
            this.button202.Name = "button202";
            this.button202.Size = new System.Drawing.Size(183, 23);
            this.button202.TabIndex = 139;
            this.button202.Text = "Use Airstrike -1280";
            this.button202.UseVisualStyleBackColor = true;
            this.button202.Click += new System.EventHandler(this.button202_Click);
            // 
            // button203
            // 
            this.button203.Enabled = false;
            this.button203.Location = new System.Drawing.Point(600, 618);
            this.button203.Name = "button203";
            this.button203.Size = new System.Drawing.Size(183, 23);
            this.button203.TabIndex = 140;
            this.button203.Text = "Use Mines -800";
            this.button203.UseVisualStyleBackColor = true;
            this.button203.Click += new System.EventHandler(this.button203_Click);
            // 
            // button204
            // 
            this.button204.Enabled = false;
            this.button204.Location = new System.Drawing.Point(600, 647);
            this.button204.Name = "button204";
            this.button204.Size = new System.Drawing.Size(183, 23);
            this.button204.TabIndex = 141;
            this.button204.Text = "Use Radar -400";
            this.button204.UseVisualStyleBackColor = true;
            this.button204.Click += new System.EventHandler(this.button204_Click);
            // 
            // button205
            // 
            this.button205.Location = new System.Drawing.Point(1083, 587);
            this.button205.Name = "button205";
            this.button205.Size = new System.Drawing.Size(141, 82);
            this.button205.TabIndex = 142;
            this.button205.Text = "Ready to Play";
            this.button205.UseVisualStyleBackColor = true;
            this.button205.Click += new System.EventHandler(this.button205_Click);
            // 
            // GameBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleTurquoise;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.button205);
            this.Controls.Add(this.button204);
            this.Controls.Add(this.button203);
            this.Controls.Add(this.button202);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button201);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.chat);
            this.Controls.Add(this.actionLog);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GameBoard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BattleBoxes";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GameBoard_Close);
            this.Load += new System.EventHandler(this.GameBoard_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button91;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.Button button58;
        private System.Windows.Forms.Button button59;
        private System.Windows.Forms.Button button60;
        private System.Windows.Forms.Button button61;
        private System.Windows.Forms.Button button62;
        private System.Windows.Forms.Button button63;
        private System.Windows.Forms.Button button64;
        private System.Windows.Forms.Button button65;
        private System.Windows.Forms.Button button66;
        private System.Windows.Forms.Button button67;
        private System.Windows.Forms.Button button68;
        private System.Windows.Forms.Button button69;
        private System.Windows.Forms.Button button70;
        private System.Windows.Forms.Button button71;
        private System.Windows.Forms.Button button72;
        private System.Windows.Forms.Button button73;
        private System.Windows.Forms.Button button74;
        private System.Windows.Forms.Button button75;
        private System.Windows.Forms.Button button76;
        private System.Windows.Forms.Button button77;
        private System.Windows.Forms.Button button78;
        private System.Windows.Forms.Button button79;
        private System.Windows.Forms.Button button80;
        private System.Windows.Forms.Button button81;
        private System.Windows.Forms.Button button82;
        private System.Windows.Forms.Button button83;
        private System.Windows.Forms.Button button84;
        private System.Windows.Forms.Button button85;
        private System.Windows.Forms.Button button86;
        private System.Windows.Forms.Button button87;
        private System.Windows.Forms.Button button88;
        private System.Windows.Forms.Button button89;
        private System.Windows.Forms.Button button90;
        private System.Windows.Forms.Button button92;
        private System.Windows.Forms.Button button93;
        private System.Windows.Forms.Button button94;
        private System.Windows.Forms.Button button95;
        private System.Windows.Forms.Button button96;
        private System.Windows.Forms.Button button97;
        private System.Windows.Forms.Button button98;
        private System.Windows.Forms.Button button99;
        private System.Windows.Forms.Button button100;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button button101;
        private System.Windows.Forms.Button button102;
        private System.Windows.Forms.Button button103;
        private System.Windows.Forms.Button button104;
        private System.Windows.Forms.Button button105;
        private System.Windows.Forms.Button button106;
        private System.Windows.Forms.Button button107;
        private System.Windows.Forms.Button button108;
        private System.Windows.Forms.Button button109;
        private System.Windows.Forms.Button button110;
        private System.Windows.Forms.Button button111;
        private System.Windows.Forms.Button button112;
        private System.Windows.Forms.Button button113;
        private System.Windows.Forms.Button button114;
        private System.Windows.Forms.Button button115;
        private System.Windows.Forms.Button button116;
        private System.Windows.Forms.Button button117;
        private System.Windows.Forms.Button button118;
        private System.Windows.Forms.Button button119;
        private System.Windows.Forms.Button button120;
        private System.Windows.Forms.Button button121;
        private System.Windows.Forms.Button button122;
        private System.Windows.Forms.Button button123;
        private System.Windows.Forms.Button button124;
        private System.Windows.Forms.Button button125;
        private System.Windows.Forms.Button button126;
        private System.Windows.Forms.Button button127;
        private System.Windows.Forms.Button button128;
        private System.Windows.Forms.Button button129;
        private System.Windows.Forms.Button button130;
        private System.Windows.Forms.Button button131;
        private System.Windows.Forms.Button button132;
        private System.Windows.Forms.Button button133;
        private System.Windows.Forms.Button button134;
        private System.Windows.Forms.Button button135;
        private System.Windows.Forms.Button button136;
        private System.Windows.Forms.Button button137;
        private System.Windows.Forms.Button button138;
        private System.Windows.Forms.Button button139;
        private System.Windows.Forms.Button button140;
        private System.Windows.Forms.Button button141;
        private System.Windows.Forms.Button button142;
        private System.Windows.Forms.Button button143;
        private System.Windows.Forms.Button button144;
        private System.Windows.Forms.Button button145;
        private System.Windows.Forms.Button button146;
        private System.Windows.Forms.Button button147;
        private System.Windows.Forms.Button button148;
        private System.Windows.Forms.Button button149;
        private System.Windows.Forms.Button button150;
        private System.Windows.Forms.Button button151;
        private System.Windows.Forms.Button button152;
        private System.Windows.Forms.Button button153;
        private System.Windows.Forms.Button button154;
        private System.Windows.Forms.Button button155;
        private System.Windows.Forms.Button button156;
        private System.Windows.Forms.Button button157;
        private System.Windows.Forms.Button button158;
        private System.Windows.Forms.Button button159;
        private System.Windows.Forms.Button button160;
        private System.Windows.Forms.Button button161;
        private System.Windows.Forms.Button button162;
        private System.Windows.Forms.Button button163;
        private System.Windows.Forms.Button button164;
        private System.Windows.Forms.Button button165;
        private System.Windows.Forms.Button button166;
        private System.Windows.Forms.Button button167;
        private System.Windows.Forms.Button button168;
        private System.Windows.Forms.Button button169;
        private System.Windows.Forms.Button button170;
        private System.Windows.Forms.Button button171;
        private System.Windows.Forms.Button button172;
        private System.Windows.Forms.Button button173;
        private System.Windows.Forms.Button button174;
        private System.Windows.Forms.Button button175;
        private System.Windows.Forms.Button button176;
        private System.Windows.Forms.Button button177;
        private System.Windows.Forms.Button button178;
        private System.Windows.Forms.Button button179;
        private System.Windows.Forms.Button button180;
        private System.Windows.Forms.Button button181;
        private System.Windows.Forms.Button button182;
        private System.Windows.Forms.Button button183;
        private System.Windows.Forms.Button button184;
        private System.Windows.Forms.Button button185;
        private System.Windows.Forms.Button button186;
        private System.Windows.Forms.Button button187;
        private System.Windows.Forms.Button button188;
        private System.Windows.Forms.Button button189;
        private System.Windows.Forms.Button button190;
        private System.Windows.Forms.Button button191;
        private System.Windows.Forms.Button button192;
        private System.Windows.Forms.Button button193;
        private System.Windows.Forms.Button button194;
        private System.Windows.Forms.Button button195;
        private System.Windows.Forms.Button button196;
        private System.Windows.Forms.Button button197;
        private System.Windows.Forms.Button button198;
        private System.Windows.Forms.Button button199;
        private System.Windows.Forms.Button button200;
        private System.Windows.Forms.ListBox actionLog;
        private System.Windows.Forms.ListBox chat;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button201;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label timeText;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label opScoreText;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label playerScoreText;
        private System.Windows.Forms.Timer secondTick;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button button202;
        private System.Windows.Forms.Button button203;
        private System.Windows.Forms.Button button204;
        private System.Windows.Forms.Button button205;
    }
}

