﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Battleship
{
    public partial class GameBoard : Form
    {
    	private int minute;
    	private int second;

        /*
         * Valid game states:
         * 0 - Setting ship tiles
         * 1 - Our move
         * 2 - Opponent move
         * 3 - Final (is this needed?)
         */
        private GameState gamestate;
        private GameLogic game;
        public enum TileType { Ship, Hit, Miss, Mine, Empty };
        public enum GameState { Setup, Player, Opponent, End};
        private bool mineLock = false; // When less then 16 tiles are left, we activate a lock for fairness

        public GameBoard()
        {
            InitializeComponent();
        }

        private void GameBoard_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Button tile = (Button)getPlayerTile(i, j);
                    if (tile != null)
                    {
                        tile.Text = " ";
                        tile.FlatStyle = FlatStyle.Flat;
                        tile.BackColor = Color.Blue;
                    }
                }
            }
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Button tile = (Button)getOpponentTile(i, j);
                    if (tile != null)
                    {
                        tile.Text = " ";
                        tile.FlatStyle = FlatStyle.Flat;
                        tile.BackColor = Color.Blue;
                    }
                }
            }

            game = new GameLogic(true,this);
            label35.Text = "Set your ships on the board! " + (GameLogic.MAX_SHIPS - game.localShipCount) +  " remaining.";
            setMoveTime();
            startMoveTimer();
            gamestate = 0;
                      
        }

        /*
         * METHODS FOR PLAYER BOARD
         */ 

        public Control getPlayerTile(int x, int y)
        {
            
            return tableLayoutPanel1.GetControlFromPosition(x,y);
        }

        public int[] getPlayerCord(Button button)
        {
            for(int i = 0; i < 10; i++)
            {
                for(int j = 0; j < 10; j++)
                {
                    Button isMe = (Button)getPlayerTile(i, j);
                    if (button.Equals(isMe))
                    {
                        int[] retVal = new int[2];
                        retVal[0] = i;
                        retVal[1] = j;
                        return retVal;
                    }
                }
            }
            return null;
        }

        public TileType getPlayerShipType(int x, int y)
        {
            return game.player[y, x];
        }

        public void setPlayerShipType(int x, int y, TileType type)
        {
            Button myTile = (Button)getPlayerTile(x, y);
            if (type == TileType.Ship)
            {
                myTile.BackColor = Color.Gray;
                myTile.Enabled = true;
                if(gamestate == GameState.Setup)
                {
                    label35.Text = "Set your ships on the board! " + (GameLogic.MAX_SHIPS-game.localShipCount) + " remaining.";
                }
            }
            else if (type == TileType.Hit)
            {
                myTile.BackColor = Color.Red;
                myTile.Enabled = false;
            }
            else if (type == TileType.Miss)
            {
                myTile.BackColor = Color.White;
                myTile.Enabled = false;
            }
            else if (type == TileType.Mine)
            {
                myTile.BackColor = Color.Green;
                myTile.Enabled = false;
            }
            else if (type == TileType.Empty)
            {
                myTile.BackColor = Color.Blue;
                myTile.Enabled = true;
                if (gamestate == GameState.Setup)
                {
                    label35.Text = "Set your ships on the board! " + (GameLogic.MAX_SHIPS - game.localShipCount) + " remaining.";
                }
            }
            else
            {
                throw new Exception("A severe error in the game logic has occured.");
            }
        }
        /*
         * END PLAYER BOARD METHODS
         */


        /*
         * METHODS FOR OPPONENT BOARD
         */

        public int[] getOpponentCord(Button button)
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Button isMe = (Button)getOpponentTile(i, j);
                    if (button.Equals(isMe))
                    {
                        int[] retVal = new int[2];
                        retVal[0] = i;
                        retVal[1] = j;
                        return retVal;
                    }
                }
            }
            return null;
        }

        public Control getOpponentTile(int x, int y)
        {
            return tableLayoutPanel2.GetControlFromPosition(x, y);
        }

        public TileType getOpponentShipType(int x, int y)
        {
            return game.opponent[y, x];
        }

        public void setOpponentShipType(int x, int y, TileType type)
        {
            Button myTile = (Button)getOpponentTile(x, y);
            if (type == TileType.Ship)
            {
                myTile.BackColor = Color.Gray;
                myTile.Enabled = true;
            }
            else if (type == TileType.Hit)
            {
                myTile.BackColor = Color.Red;
                myTile.Enabled = false;
            }
            else if (type == TileType.Miss)
            {
                myTile.BackColor = Color.White;
                myTile.Enabled = false;
            }
            else if (type == TileType.Mine)
            {
                myTile.BackColor = Color.Green;
                myTile.Enabled = false;
            }
            else if (type == TileType.Empty)
            {
                myTile.BackColor = Color.Blue;
                myTile.Enabled = true;
            }
            else
            {
                throw new Exception("A severe error in the game logic has occured.");
            }
        }

        /*
         * END OPPONENT BOARD METHODS
         */

        public bool setMoveTime(int seconds = 120) {
        	if(seconds <= 0) {
        		return false;
        	}
        	int ms = seconds * 1000;
        	minute = seconds / 60;
        	second = seconds % 60;
        	if(second >= 10){
				timeText.Text = minute + ":" + second;
			} else {
				timeText.Text = minute + ":" + "0" + second;
			}
        	timeText.ForeColor = Color.Black;
        	return true;
        }
        
        public void startMoveTimer(){
        	secondTick.Start();
        }
        public void stopMoveTimer()
        {
            secondTick.Stop();
        }
        void SecondTickTick(object sender, EventArgs e)
		{
            if(gamestate == GameState.Opponent)
            {
                if (GameLogic.netMode)
                {
                    if(TCPClassServer.checkStatus(TCPClassServer.state.workSocket))
                    {
                        TCPClassServer.Receive(TCPClassServer.state.workSocket);
                        TCPClassServer.receiveDone.WaitOne();
                        TCPClassServer.receiveDone.Reset();
                        parseData(TCPClassServer.state.sb.ToString());
                        TCPClassServer.state.sb.Clear();
                        stopMoveTimer();

                        opScoreText.Text = game.opponentScore.ToString();

                        setMoveTime(60);
                        startMoveTimer();
                    }
                }
                else
                {
                    if (TCPClassClient.checkStatus(TCPClassClient.client))
                    {                 
                        TCPClassClient.Receive(TCPClassClient.client);
                        TCPClassClient.receiveDone.WaitOne();
                        TCPClassClient.receiveDone.Reset();
                        parseData(TCPClassClient.state.sb.ToString());
                        TCPClassClient.state.sb.Clear();
                        stopMoveTimer();
                        opScoreText.Text = game.opponentScore.ToString();
                        setMoveTime(60);
                        startMoveTimer();
                    }
                }
            }
            // TODO: Handle game over
			second--;
			if(second < 0){
				minute--;
				if(minute < 0){
					secondTick.Stop();
                    if (GameLogic.netMode && gamestate == GameState.Player)
                    {
                        TCPClassServer.Send("T:");
                        actionLog.Items.Add("You did not make a move in time!");
                        label35.Text = "Waiting for opponent...";
                        gamestate = GameState.Opponent;
                        setMoveTime(60);
                        startMoveTimer();
                    }
                    else if (gamestate == GameState.Player)
                    {
                        TCPClassClient.Send("T:");
                        actionLog.Items.Add("You did not make a move in time!");
                        label35.Text = "Waiting for opponent...";
                        gamestate = GameState.Opponent;
                        setMoveTime(60);
                        startMoveTimer();
                    }
					return;
				}
				second = 59;
				
				
			}
			if(minute < 1) {
				timeText.ForeColor = Color.Red;
			}
			
        	if(second >= 10){
				timeText.Text = minute + ":" + second;
			} else {
				timeText.Text = minute + ":" + "0" + second;
			}
			
			
		}
        void parseData(String command)
        {
            String[] segments = command.Split(':');
            switch (segments[0])
            {
                case "S": // Regular move
                    if(game.player[Int32.Parse(segments[1]),Int32.Parse(segments[2])] == TileType.Ship)
                    {
                        game.player[Int32.Parse(segments[1]), Int32.Parse(segments[2])] = TileType.Hit;
                        setPlayerShipType(Int32.Parse(segments[2]), Int32.Parse(segments[1]), TileType.Hit);
                        game.localShipCount--;
                        game.notHit--;
                        game.opponentScore = Int32.Parse(segments[3]);
                        actionLog.Items.Add("Opponent hit your ship!");
                    }
                    if (game.player[Int32.Parse(segments[1]), Int32.Parse(segments[2])] == TileType.Empty || game.player[Int32.Parse(segments[1]), Int32.Parse(segments[2])] == TileType.Mine)
                    {
                        game.player[Int32.Parse(segments[1]), Int32.Parse(segments[2])] = TileType.Miss;
                        setPlayerShipType(Int32.Parse(segments[2]), Int32.Parse(segments[1]), TileType.Miss);
                        game.notHit--;
                        game.opponentScore = Int32.Parse(segments[3]);
                        if (game.player[Int32.Parse(segments[1]), Int32.Parse(segments[2])] == TileType.Empty)
                        {
                            actionLog.Items.Add("Opponent missed your ship!");
                        }
                        if (game.player[Int32.Parse(segments[1]), Int32.Parse(segments[2])] == TileType.Mine)
                        {
                            actionLog.Items.Add("Opponent hit a mine!");
                        }
                    }
                    break;
                case "R": // Radar
                    game.opponentScore = Int32.Parse(segments[1]);
                    actionLog.Items.Add("Opponent used radar!");
                    break; 
                case "A": // Airstrike
                    for(int i = 0; i < 10; i++)
                    {
                        if (game.player[i, Int32.Parse(segments[1])] == TileType.Ship)
                        {
                            game.player[i, Int32.Parse(segments[1])] = TileType.Hit;
                            setPlayerShipType(Int32.Parse(segments[1]), i, TileType.Hit);
                            game.localShipCount--;
                            game.notHit--;
                        }
                        if (game.player[i, Int32.Parse(segments[1])] == TileType.Empty || game.player[i, Int32.Parse(segments[1])] == TileType.Mine)
                        {
                            game.player[i, Int32.Parse(segments[1])] = TileType.Miss;
                            setPlayerShipType(Int32.Parse(segments[1]), i, TileType.Miss);
                            game.notHit--;
                        }
                        actionLog.Items.Add("Opponent used a airstike!");
                    }
                    game.opponentScore = Int32.Parse(segments[2]);
                    break;
                case "M": // Mine
                    int pos = 1;
                    for (int i = 0; i < 5; i+=2)
                    {
                        game.opponent[Int32.Parse(segments[pos + i]), Int32.Parse(segments[pos + (i + 1)])] = TileType.Mine;
                    }
                    game.opponentScore = Int32.Parse(segments[segments.Length - 1]);
                    actionLog.Items.Add("Opponent deployed mines!");
                    break;
                case "T"://Timeout
                    actionLog.Items.Add("Opponent did not make a move in time!");
                    break;
            }
            if (game.localShipCount == 0 || game.notHit == 0)
            {
                label35.Text = "The game is over.";
                gamestate = GameState.End;
                endGame();
            }
            else
            {
                label35.Text = "It's your turn!";
                gamestate = GameState.Player;
            }
        }

        private void GameBoard_Close(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        private void tileClickHandler(Button tile)
        {
            
            switch (gamestate)
            {
                case GameState.Setup:
                    int[] pt = getPlayerCord(tile);
                    if(pt == null)
                    {
                        return; // Invalid selection, ignore it
                    }
                    bool status = game.setShip(pt[0],pt[1]);
                    if (status)
                    {

                    }
                    break;
                case GameState.Player:
                    int[] pt2 = getOpponentCord(tile);
                    if (pt2 == null)
                    {
                        return; // Invalid selection, ignore it
                    }
                    bool move = game.makeMove(pt2[0], pt2[1], minute, second);
                    if (move)
                    {
                        stopMoveTimer();
                        if (game.myHits == 17)
                        {
                            gamestate = GameState.End;
                            endGame();
                        }
                        else
                        {
                            gamestate = GameState.Opponent;
                            label35.Text = "Waiting for opponent...";
                            setMoveTime(60);
                            startMoveTimer();
                        }
                    }
                    break;
                case GameState.Opponent:
                    break; // Do nothing, it isn't our turn
                case GameState.End:
                    break; // Game is over, do nothing
            }
            playerScoreText.Text = game.playerScore.ToString();
            
        }

        public void addAction(String s)
        {
            actionLog.Items.Add(s);
        }

        // This function handles starting the game after setting ships.
        private void button205_Click(object sender, EventArgs e)
        {
            if (game.verifyCount())
            {
                

                gamestate = GameState.End;
                button205.Enabled = false;
                button205.Visible = false;
                label35.Text = "Waiting for opponent...";
                stopMoveTimer();
                game.exchangeBoards();
                if (GameLogic.netMode)
                {
                    label35.Text = "It's your turn!";
                    gamestate = GameState.Player;
                    setMoveTime(60);
                    startMoveTimer();
                }
                else
                {
                    label35.Text = "Waiting for opponent...";
                    gamestate = GameState.Opponent;
                    setMoveTime(60);
                    startMoveTimer();
                }
            } else
            {
                int status = (GameLogic.MAX_SHIPS - game.localShipCount);
                if (status < 0)
                {
                    MessageBox.Show("You have too many ships on the board." + " Remove " + Math.Abs(status) + " ships.", "Invalid Ship Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                } else if (status > 0)
                {
                    MessageBox.Show("You do not have enough ships on the board." + " Add " + Math.Abs(status) + " ships.", "Invalid Ship Setup", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        // Airstrike button
        private void button202_Click(object sender, EventArgs e)
        {
            int x = 0;
            while (true)
            {
                string col = Microsoft.VisualBasic.Interaction.InputBox("Enter a column number to airstrike.", "Choose Column", "", this.Location.X, this.Location.Y);
                if (col.Equals(""))
                {
                    return;
                } else if (col.Equals("1"))
                {
                    x = 0;
                }
                else if (col.Equals("2"))
                {
                    x = 1;
                }
                else if (col.Equals("3"))
                {
                    x = 2;
                }
                else if (col.Equals("4"))
                {
                    x = 3;
                }
                else if (col.Equals("5"))
                {
                    x = 4;
                }
                else if (col.Equals("6"))
                {
                    x = 5;
                }
                else if (col.Equals("7"))
                {
                    x = 6;
                }
                else if (col.Equals("8"))
                {
                    x = 7;
                }
                else if (col.Equals("9"))
                {
                    x = 8;
                }
                else if (col.Equals("10"))
                {
                    x = 9;
                } else
                {
                    continue;
                }
                break;
            }
            if(gamestate == GameState.Player)
            {
                game.useAirStrike(x);
                stopMoveTimer();
                if(game.myHits == 17)
                {
                    gamestate = GameState.End;
                    endGame();
                } else { 
                gamestate = GameState.Opponent;
                label35.Text = "Waiting for opponent...";
                setMoveTime(60);
                startMoveTimer();
                }
            }
        }

        private void endGame()
        {
            if(game.playerScore > game.opponentScore)
            {
                UDPClass.sendUDPMessageToServer("Highscore/" + game.playerScore + "/" + (String)Properties.Settings.Default["name"]+"/");
            }
            MessageBox.Show("The game has ended.", "Game Over", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Application.Exit();
        }
        // Mines button
        private void button203_Click(object sender, EventArgs e)
        {
            if (gamestate == GameState.Player)
            {
                if (!game.useMine()) { mineLock = true; }
                stopMoveTimer();
                gamestate = GameState.Opponent;
                label35.Text = "Waiting for opponent...";
                setMoveTime(60);
                startMoveTimer();
            }
        }

        // Radar button
        private void button204_Click(object sender, EventArgs e)
        {
            if (gamestate == GameState.Player)
            {
                game.useRadar();
                stopMoveTimer();
                gamestate = GameState.Opponent;
                label35.Text = "Waiting for opponent...";
                setMoveTime(60);
                startMoveTimer();
            }
        }

        public void updatePowerUps()
        {
            if(game.playerScore >= 1280)
            {
                button202.Enabled = true;
            } else
            {
                button202.Enabled = false;
            }

            if (game.playerScore >= 800 && !mineLock)
            {
                button203.Enabled = true;
            }
            else
            {
                button203.Enabled = false;
            }

            if (game.playerScore >= 400)
            {
                button204.Enabled = true;
            }
            else
            {
                button204.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button2_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button3_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button4_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button5_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button6_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button7_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button8_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button9_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button10_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button11_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button12_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button13_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button14_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button15_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button16_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button17_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button18_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button19_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button20_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button21_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button22_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button23_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button24_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button25_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button26_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button27_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button28_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button29_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button30_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button31_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button32_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button33_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button34_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button35_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button36_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button37_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button38_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button39_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button40_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button41_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button42_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button43_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button44_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button45_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button46_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button47_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button48_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button49_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button50_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button51_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button52_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button53_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button54_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button55_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button56_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button57_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button58_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button59_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button60_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button61_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button62_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button63_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button64_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button65_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button66_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button67_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button68_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button69_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button70_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button71_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button72_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button73_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button74_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button75_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button76_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button77_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button78_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button79_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button80_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button81_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button82_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button83_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button84_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button85_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button86_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button87_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button88_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button89_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button90_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button91_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button92_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button93_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button94_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button95_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button96_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button97_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button98_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button99_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button100_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button101_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button102_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button103_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button104_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button105_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button106_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button107_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button108_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button109_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button110_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button111_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button112_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button113_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button114_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button115_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button116_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button117_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button118_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button119_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button120_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button121_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button122_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button123_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button124_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button125_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button126_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button127_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button128_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button129_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button130_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button131_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button132_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button133_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button134_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button135_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button136_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button137_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button138_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button139_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button140_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button141_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button142_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button143_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button144_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button145_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button146_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button147_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button148_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button149_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button150_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button151_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button152_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button153_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button154_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button155_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button156_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button157_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button158_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button159_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button160_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button161_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button162_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button163_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button164_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button165_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button166_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button167_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button168_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button169_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button170_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button171_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button172_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button173_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button174_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button175_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button176_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button177_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button178_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button179_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button180_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button181_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button182_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button183_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button184_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button185_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button186_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button187_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button188_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button189_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button190_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button191_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button192_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button193_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button194_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button195_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button196_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button197_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button198_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button199_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }
        private void button200_Click(object sender, EventArgs e) { tileClickHandler((Button)sender); }


    }
}
