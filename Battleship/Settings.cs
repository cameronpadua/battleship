﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Battleship
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!textBox1.Text.Contains("~"))
            {
                Properties.Settings.Default["name"] = textBox1.Text;
                Properties.Settings.Default.Save();
            }
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            textBox1.Text = (String)Properties.Settings.Default["name"];
        }
    }
}
