﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using Microsoft.VisualBasic;
using System.Diagnostics;

namespace Battleship
{

    public partial class MainMenu : Form
    {

        private bool connected = false;

        public MainMenu()
        {
            InitializeComponent();
        }

        private void MainMenu_Shown(Object sender, EventArgs e)
        {
            findServer();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            GameBoard c = new GameBoard();
            c.Show();
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (connected)
            {
                MatchmakingScreen a = new MatchmakingScreen();
                this.Hide();
                a.Show();
            } else
            {
                findServer();
            }
        }

        private void findServer()
        {
            IPAddress serverIp = IPAddress.None;
            byte[] buffer = new byte[1024];
            IPHostEntry entry = Dns.Resolve(Dns.GetHostName());
            Console.WriteLine(entry.ToString());
            foreach (IPAddress address in entry.AddressList)
            {
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                socket.Bind(new IPEndPoint(address, 0));
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Broadcast, 5200);
                socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
                socket.SendTo(Encoding.ASCII.GetBytes("Server?/"), remoteEP);
                Console.Write("Client BroadCasted to " + address.ToString() + "\n");
                socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 1000);
                try
                {
                    EndPoint point2 = new IPEndPoint(IPAddress.Any, 5201);
                    socket.ReceiveFrom(buffer, ref point2);
                    Console.Write("Client got '" + UDPClass.GetStringFromBuffer(buffer) + "' from " + point2.ToString() + Environment.NewLine);
                    serverIp = IPAddress.Parse(point2.ToString().Split(":".ToCharArray(), 2)[0]);
                    UDPClass.setServerIPAndPort(serverIp, 5200);
                    ServerStatusLabel.Text = "Connected to Server: " + serverIp.ToString();
                    connected = true;
                    break;
                }
                catch
                {
                }
                socket.Close();
            }
            //IF Server did not respond to broadcasts, have user enter a IP address
            if (serverIp == IPAddress.None)
            {
                while (true)
                {
                    string serverIPAddress = Microsoft.VisualBasic.Interaction.InputBox("The program was not able to find the server, please enter a valid server IP to continue.", "Warning: Server Not Found", "", this.Location.X, this.Location.Y);
                    if(serverIPAddress == "")
                    {
                        break;
                    }
                    if (IPAddress.TryParse(serverIPAddress, out serverIp))
                    {
                        UDPClass.setServerIPAndPort(serverIp, 5200);
                        Socket socket = UDPClass.sendUDPMessageToServer("Server?/");
                        Console.WriteLine("Sending to Server");
                        try
                        {
                            EndPoint point2 = new IPEndPoint(IPAddress.Any, ((IPEndPoint)socket.LocalEndPoint).Port);
                            //socket.Bind(point2);
                            socket.ReceiveFrom(buffer, ref point2);
                            Console.Write("Client got '" + UDPClass.GetStringFromBuffer(buffer) + "' from " + point2.ToString() + Environment.NewLine);
                            ServerStatusLabel.Text = "Connected to Server: " + serverIp.ToString();
                            connected = true;
                            break;
                        }
                        catch
                        {

                        }
                    }
                }
            }
        }

        private void MainMenu_Load(object sender, EventArgs e)
        {
            // Patch to solve ghost process issue
            Process[] localByName = Process.GetProcessesByName("battleship");
            foreach(Process i in localByName) {
                if(i.Id != Process.GetCurrentProcess().Id)
                {
                    i.Kill();
                }
            }
        }

        // Leaderboard button
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (connected)
            {
                Leaderboard a = new Leaderboard();
                a.Show();
            }
            else
            {
                findServer();
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
                Settings a = new Settings();
                a.Show();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
