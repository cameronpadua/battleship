﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

namespace Server
{
    class Server
    {
        public class playerCard
        {
            public string playerName = "";
            public String playerIP = "";
            public int playerPort = 0;
            public playerCard(string playerN, string _playerIP, int _playerPort)
            {
                playerName = playerN;
                playerIP = _playerIP;
                playerPort = _playerPort;
            }

        }
        static bool playerQueued = false;
        static string playerQueuedIP = "";
        static int playerQueuedPort = 0;
        private static Dictionary<string, playerCard> playerList = new Dictionary<string, playerCard>();
        private static string highScoreString = "";
        static void Main(string[] args)
        {
            const int Port = 5200;
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            Console.Write("Running server..." + Environment.NewLine);
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    Console.Write("Server IP:" + ip.ToString() + Environment.NewLine);
                }
            }
            
            server.Bind(new IPEndPoint(IPAddress.Any, Port));
            String recieved = "";

            while (true)
            {
                IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
                EndPoint tempRemoteEP = (EndPoint)sender;
                byte[] buffer = new byte[1024];
                server.ReceiveFrom(buffer, ref tempRemoteEP);
                //gets data sent by client and add the ip:port to the end
                recieved = GetStringFromBuffer(buffer) + tempRemoteEP.ToString()+ "/";
                Console.Write("Server got '" + recieved +"' from " + tempRemoteEP.ToString() + Environment.NewLine);
                new Thread(ParseRequest).Start(recieved);
                
            }
        }

    private static void ParseRequest(object passedIn)
        {
            //Takes in a String that represent the request.
            String Request = (String)passedIn;
            String[] segements = Request.Split('/');
            switch (segements[0])
            {
                //Tested
                case "Server?":
                    Console.WriteLine("Entering Server Response");
                    serverReply(segements);
                    break;
                //Tested, will redo 
                case "Highscore":
                    //CommandStructure:
                    //Highscore/Score/Name/IP:sentFromPort/
                    addHighScore(segements);
                    break;
                //Tested, want to redo
                case "Disconnect":
                    //CommandStructure:
                    //Disconnect/IP:sentFromPort/
                    playerQueued = false;
                    playerQueuedIP = "";
                    break;
                //Not Tested
                case "Host":
                    //CommandStructure:
                    //Host/Name/availablePort/IP:sentFromPort/
                    addHostGameList(segements);
                    break;
                //Not Tested
                case "RemoveHost":
                    //CommandStructure:
                    //RemoveHost/Name/availablePort/IP:sentFromPort/
                    removeHostGameList(segements);
                    break;
                //TO DO
                case "ListGames":
                    listGames(segements);
                    break;
                //Tested
                case "QuickConnection":
                    //CommandStructure:
                    //QuickConnection/availablePort/IP:sentFromPort/
                    if (playerQueued)
                    {
                        Console.WriteLine("Connecting Players");
                        connectionHandler(segements);
                    }
                    else
                    {
                        tellOpenPort(segements);
                    }
                    break;
                case "getHighScores":
                    getHighScores(segements);
                    break;
            }
        }
        private static void getHighScores(object passedIn)
        {
            String[] segs = (String[])passedIn;
            Socket temp = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            EndPoint requestor = new IPEndPoint(parseIP(segs[1]), parsePort(segs[1]));
            temp.SendTo(Encoding.ASCII.GetBytes(highScoreString), requestor);
            temp.Close();
        }
        private static void listGames(object passedIn)
        {
            /*
            String[] segs = (String[])passedIn;
            var binFormatter = new BinaryFormatter();
            var mStream = new MemoryStream();
            binFormatter.Serialize(mStream, playerList);

            //This gives you the byte array.
            byte[] send = mStream.ToArray();

            mStream.Write(send, 0, send.Length);
            mStream.Position = 0;

            Dictionary<string, playerCard> a = binFormatter.Deserialize(mStream) as Dictionary<string, playerCard>;
            Console.WriteLine(a.ToString());
            byte[] buffer = new byte[1024];
            Socket temp = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            EndPoint requestor = new IPEndPoint(parseIP(segs[1]), parsePort(segs[1]));
            Console.WriteLine(requestor.ToString());
            
            temp.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 15000);
            byte[] replybuffer = new byte[1024];
            temp.Bind(new IPEndPoint(IPAddress.Any, 0));
            try
            {
                
                Console.Write("Sending Reply to Host Request \n");
                temp.SendTo(send, requestor);
                temp.ReceiveFrom(buffer, ref requestor);
                if (playerList.Remove(GetStringFromBuffer(buffer)))
                {
                    temp.SendTo(Encoding.ASCII.GetBytes("OK"), requestor);
                }
                else
                {
                    temp.SendTo(Encoding.ASCII.GetBytes("Fail"), requestor);
                }
            }
            catch
            {

            }
            temp.Close();*/
            String[] segs = (String[])passedIn;
            string availableGames = "";
            foreach(string key in playerList.Keys)
            {
                availableGames += key + ":" + playerList[key].playerName + ":";
            }
            byte[] buffer = new byte[1024];
            Socket temp = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            EndPoint requestor = new IPEndPoint(parseIP(segs[1]), parsePort(segs[1]));
            Console.WriteLine(requestor.ToString());

            temp.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 120000);
            temp.Bind(new IPEndPoint(IPAddress.Any, 5500));
            try
            {
                EndPoint a = new IPEndPoint(IPAddress.Any, 5500);
                Console.Write("Sending Reply to Host Request \n");
                temp.SendTo(Encoding.ASCII.GetBytes(availableGames), requestor);
                temp.ReceiveFrom(buffer, ref a);
                String[] segs2 = GetStringFromBuffer(buffer).Split(':');
                if (playerList.ContainsKey(segs2[1]))
                {
                    
                    temp.SendTo(Encoding.ASCII.GetBytes("OK/"+segs2[1]+"/"+ playerList[segs2[1]].playerPort+"/"), requestor);
                    playerList.Remove(segs2[1]);
                }
                else
                {
                    temp.SendTo(Encoding.ASCII.GetBytes("Fail"), requestor);
                }
            }
            catch
            {
               
            }


            }
        private static void tellOpenPort(object passedIn)
        {
            String[] segs = (String[])passedIn;
            //If no player was queued, tell requestor to open port.
            byte[] buffer = new byte[1024];

            Socket temp = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint requestor = new IPEndPoint(parseIP(segs[2]), parsePort(segs[2]));
            Console.Write("Sending Reply to " + segs[2] + Environment.NewLine);
            temp.SendTo(Encoding.ASCII.GetBytes("OpenPort"), requestor);
            temp.Close();

            Console.WriteLine("Waiting for second player");
            playerQueuedIP = String.Copy(parseIP(segs[2]).ToString());
            playerQueuedPort = Int32.Parse(segs[1]);
            playerQueued = true;
        }
        private static void addHostGameList(object passedIn)
        {
            String[] segs = (String[])passedIn;
            String playerIP = String.Copy(parseIP(segs[3]).ToString());
            playerList.Add(playerIP, new playerCard(segs[1], playerIP, Int32.Parse(segs[2])));

            Socket temp = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint requestor = new IPEndPoint(parseIP(segs[3]), parsePort(segs[3]));

            temp.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 5000);
            byte[] replybuffer = new byte[1024];
            try
            {
                Console.Write("Sending Reply to Host Request \n");
                temp.SendTo(Encoding.ASCII.GetBytes("HostRecieved"), requestor);
            }
            catch
            {
                
            }
        }
        private static void removeHostGameList(object passedIn)
        {
            String[] segs = (String[])passedIn;
            String playerIP = String.Copy(parseIP(segs[3]).ToString());
            playerList.Remove(playerIP);

            Socket temp = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint requestor = new IPEndPoint(parseIP(segs[2]), parsePort(segs[2]));

            temp.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 5000);
            byte[] replybuffer = new byte[1024];
            try
            {
                Console.Write("Sending Reply to Host Remove Request \n");
                temp.SendTo(Encoding.ASCII.GetBytes("HostRemoveRequestRecieved"), requestor);
            }
            catch
            {

            }
        }
        /*serverReply
         * 
         * 
         * 
         */
        private static void serverReply(object passedIn)
        {
            //Gets Segments from thread passed in varaible
            //Seg 0 should be Request Type. IE Server?
            //Seg 1 should be the IP and port of the Requestor seperated by a :
            String[] segs = (String[])passedIn;
            //Send Confirmation that Server was found
            Socket temp = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint requestor = new IPEndPoint(parseIP(segs[1]), parsePort(segs[1]));
            try
            {
                temp.Bind(new IPEndPoint(IPAddress.Any, parsePort(segs[1])));
            }
            catch
            {

            }
            
            Console.Write("Sending Reply to " + segs[1] + Environment.NewLine);
            temp.SendTo(Encoding.ASCII.GetBytes("RequestRecieved"), requestor);
            //Close socket
            temp.Close();
        }
        private static void connectionHandler(object passedIn)
        {
            string[] segs = (String[])passedIn;
            string otherIP = String.Copy(playerQueuedIP);

            playerQueued = false;
            playerQueuedIP = "";

            Socket temp = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint requestor = new IPEndPoint(parseIP(segs[2]), parsePort(segs[2]));

            temp.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 5000);
            byte[] replybuffer = new byte[1024];
            try
            {
                Console.Write("Sending Reply Connection to Request \n");
                temp.SendTo(Encoding.ASCII.GetBytes("ConnectTo/" + otherIP + "/"+playerQueuedPort+"/"), requestor);
            }
            catch
            {
                //Timout. No client answered.
            }

            //Close socket
            temp.Close();
        }
        /*addHighScore
         *Add a new highscore to a text file of highscores. If there is no highscore
         * file, create one with 10 placeholders and the new highscore from the user
         *
         * Input: An object that is a string array containing a Name and a highscore 
         * Output: Nothing
         */
        private static void addHighScore(object passedIn)
        {
            String[] segs = (String[])passedIn;
            highScoreString = highScoreString + segs[2] + ":" + segs[1] + "~";
        }
        /*parseIP
         * Takes in a IPaddress:Port string a extracts the IP address
         * 
         * Input: IPaddress:Port string
         * Output: IPAddress ip
         */
        private static IPAddress parseIP(string passedIn)
        {
            //Seperates an IP from Port string. For Example 1.2.3.4:123 = 1.2.3.4
            return IPAddress.Parse(passedIn.Split(":".ToCharArray(), 2)[0]);
        }
        /*parsePort
         * Takes in a IPaddress:Port string a extracts the port
         * 
         * Input: IPaddress:Port string
         * Output: Int port number
         */
        private static int parsePort(string passedIn)
        {
            //Seperates an Port from the IP string. For Example 1.2.3.4:123 = 123
            return Int32.Parse(passedIn.Substring(passedIn.IndexOf(':') + 1));
        }
        
        /*GetStringFromBuffer
         * Takes in byte buffer and converts it into a string by finding hte first trailing 
         * 0 byte on the end of the array and convert everything before into a string.
         * 
         * Input: byte[] buffer
         * output: String 
         */
        private static String GetStringFromBuffer(byte[] buffer)
        {
            //Gets the String from a byte buffer. Removes all trailing 0 bytes.
            int i = buffer.Length - 1;
            //Find first non zero byte
            while (buffer[i] == 0)
                --i;
            //create a temporary byte array to store the contents of the byte string
            byte[] RetVal = new byte[i + 1];
            Array.Copy(buffer, RetVal, i + 1);
            return Encoding.ASCII.GetString(RetVal); ;

        }
    }
}